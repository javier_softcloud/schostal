// ================================================
// Puerto
// ================================================

process.env.PORT = process.env.PORT || 3002;

// ================================================
// Puerto
// ================================================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

// ================================================
// Vencimiento del token
// ================================================
process.env.CADUCIDAD_TOKEN = '24h';

// ================================================
// Semilla
// ================================================
process.env.SEED = process.env.SEED || 'este-es-el-seed';

// ================================================
// DB
// ================================================
let urlDB;

if(process.env.NODE_ENV === "dev"){
  urlDB = "mongodb://root:cornelio96@ds155263.mlab.com:55263/hostalsc";
}
else{
  urlDB = process.env.MONGO_URI;
}

process.env.URLDB = urlDB;

// ================================================
// Google CLIENT_ID
// ================================================

process.env.CLIENT_ID = process.env.CLIENT_ID || '842447740012-qcsrufkjdn876qvhcbkb7qbki8ui74v0.apps.googleusercontent.com';

// ================================================
// Google SECRET
// ================================================
process.env.SECRET = process.env.SECRET || 'XDnZOFEyxwvBNdqsryipevwG';
