const mongoose = require('mongoose');

let roles = {
  values: ['ADMIN_ROLE','ROOT','RECEP_ROLE'],
  message: '{VALUE} no es un rol valido'
}

let Schema = mongoose.Schema;

let adminSchema = new Schema({
  idhotel: {
    type: Schema.ObjectId,
    ref: "Hotel",
  },
  nombre: {
    type: String,
    required: [true, 'El nombre es requerido']
  },
  apPaterno: {
    type: String,
    required: [true, 'El apellido paterno es requerido']
  },
  apMaterno: {
    type: String
  },
  email: {
    type: String,
    required: [true, 'El correo es requerido']
  },
  password: {
    type: String,
    required: [true, 'La contraseña es requerida']
  },
  role: {
    type: String,
    default: 'ADMIN_ROLE',
    enum: roles
  },
  img: {
    type: String
  },
  accessAdministradores:{
    type: Boolean
  },
  accessHoteles: {
    type: Boolean
  },
  accessHabitaciones: {
    type: Boolean
  },
  accessContactos: {
    type: Boolean
  },
  accessClientes: {
    type: Boolean
  },
  accessCamas: {
    type: Boolean
  },
  accessReservaciones: {
    type: Boolean
  },
  accessTemporadas: {
    type: Boolean
  }
});

adminSchema.methods.toJSON = function () {
  let admin = this;
  let adminObject = admin.toObject();

  if(adminObject.role === 'ADMIN_ROLE'){
    adminObject.img = adminObject.img !== null && adminObject.img !== undefined ? `/uploads/admin/${adminObject.img}` : null;
  }
  else if(adminObject.role === 'RECEP_ROLE'){
    adminObject.img = adminObject.img !== null && adminObject.img !== undefined ? `/uploads/recepcionista/${adminObject.img}` : null;
  }

  delete adminObject.password;

  return adminObject;
}


module.exports = mongoose.model('Admin', adminSchema);
