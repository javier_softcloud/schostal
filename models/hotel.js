const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let hotelSchema = new Schema({
    nombre: {
        type: String,
        required: true
    },
    direccion: {
        type: String,
        required: true
    },
    tipo: {
        type: String,
        required: true
    },
    politicas_ninios: {
        type: String,
        required: true
    },
    max_age_children: {
        type: Number,
        required: true
    },
    checkin: {
        type: String,
        default: '12:00:00'
    },
    checkout: {
        type: String,
        default: '17:00:00'
    },
    servicios: [String],
    galeria: [String]
});

hotelSchema.methods.toJSON = function(){
    let hotel = this;
    let hotelObject = hotel.toObject();
    
    let aux = [];
    for(let foto of hotelObject.galeria){
        aux.push(`/uploads/hotel/${foto}`);
    }

    hotelObject.galeria = aux;
    
    return hotelObject;
}

module.exports = mongoose.model('Hotel', hotelSchema);