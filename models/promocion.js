const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let promocionSchema = new Schema({
    idhotel: {
        type: Schema.ObjectId,
        ref: 'Hotel',
        required: true
    },
    tipo: {
        type: String,
        required: true
    },
    terminos: {
        type: String,
        required: true
    },
    fechaInicio: {
        type: Date,
        required: true
    },
    fechaFinal: {
        type: Date,
        required: true
    },
    porcentajeDesc: {
        type: Number
    } 
});

module.exports = mongoose.model('Promocion', promocionSchema);