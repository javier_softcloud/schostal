const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let camaSchema = new Schema({
    idhotel: {
        type: Schema.ObjectId,
        ref: "Hotel",
        required: true
    },
    nombre: {
        type: String,
        required: [true, 'El tipo de cama es requerdio']
    },
    ocupacion: {
        type: Number,
        required: [true, 'La ocupación de la cama es requerida']
    },
    tipo: {
        type: String,
        required: true
    },
    total: {
        type: Number,
        default: 1
    },
    ocupadas: {
        type: Number,
        default: 0
    },
    asignadas: {
        type: Number,
        default: 0
    }
});

module.exports = mongoose.model('Cama', camaSchema);