const mongoose = require('mongoose');


let roles = {
    values: ['USER_ROLE'],
    message: '{VALUE} no es un rol valido'
}

let Schema = mongoose.Schema;

let usuarioSchema = new Schema({
  nombre: {
    type: String,
    required: [true, 'El nombre es requerido']
  },
  apPaterno: {
    type: String,
    required: [true, 'El apellido paterno es requerido']
  },
  apMaterno: {
    type: String,
      required: [true, 'El apellido materno es requerido']

  },
  fechaNacimiento: {
    type: Date,
      default:null
  },
  telefono: {
    type: String,
      default:null
  },
    calle:{
        type: String,
        default:null
    },
    numero:{
        type: String,
        default:null
    },
    ciudad:{
        type: String,
        default:null
    },
    pais:{
        type: String,
        default:null
    },
    codigoPostal:{
        type: String,
        default:null
    },
    estado:{
        type: String,
        default:null
    },
  email: {
    type: String,
    unique: true,
    required: [true, 'El correo es requerido']
  },
  password: {
    type: String,
    required: [true, 'La contraseña es requerida']
  },
  img: {
    type: String,
    default: null
  },
  role: {
    type: String,
    default: 'USER_ROLE',
    enum: roles
  },
  google:{
    default: false,
    type: Boolean
  },
  idOpenpay:{
    type:String
  }
});

usuarioSchema.set('timestamps', {
    createdAt: true,
    updatedAt: true
});
usuarioSchema.methods.toJSON = function () {
  let user = this;
  let userObject = user.toObject();

  if(userObject.img != null)
    userObject.img = `/uploads/usuario/${userObject.img}`;
  delete userObject.password;
  return userObject;
}

module.exports = mongoose.model('Usuario', usuarioSchema);
