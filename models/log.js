const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let logSchema = new Schema({
    idhotel: {
        type: Schema.ObjectId,
        ref: "Hotel",
        required: true
    },
    actividad: {
        type: String,
        required: true
    },
    fecha: {
        type: Date,
        required: true
    },
    idadmin: {
        type: Schema.ObjectId,
        ref: "Admin",
        required: true
    }
});

module.exports = mongoose.model("Log", logSchema);