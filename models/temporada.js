const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let tipos = {
    values: ['NORMAL', 'REV', 'BOUT'],
    message: '{VALUE} no es un tipo valido'
}

let temporadaSchema = new Schema({
    idhotel: {
        type: Schema.ObjectId,
        ref: 'Hotel'
    },
    nombre: {
        type: String
    },
    tipo: {
        type: String,
        default: 'NORMAL',
        enum: tipos
    },
    fechaInicio: {
        type: Date,
        required: true
    },
    fechaFinal: {
        type: Date,
        required: true
    }
});

module.exports = mongoose.model('Temporada', temporadaSchema);