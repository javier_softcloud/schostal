const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let consumibleSchema = new Schema({
    idhotel: {
        type: Schema.ObjectId,
        ref: "Hotel",
        required: true
    },
    nombre: {
        type: String,
        required: true
    },
    precio: {
        type: Number,
        required: true
    },
    cantidad: {
        type: Number,
        required: true
    },
    descripcion: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Consumible', consumibleSchema);