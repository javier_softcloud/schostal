const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let reservaSchema = new Schema({
    idhotel:{
        type: Schema.ObjectId,
        ref: 'Hotel',
        required: true
    },
    habs:[{
        type: Schema.ObjectId,
        ref: 'Habitacion',
        required: true
    }],
    ocupacionAdultos:{
        type: Number,
        required: true
    },
    ocupacionNinos:{
        type: Number,
        required: true
    },
    status:{
        type: String,
        required: true
    },
    total:{
        type: Number,
        required: true
    },
    idusuario:{
        type: Schema.ObjectId,
        ref: 'Usuario',
        required: true
    },
    checkin:{
        type: Date,
        required: true
    },
    checkout:{
        type: Date,
        required: true
    },
    guests: [String],
    creditCard: {
        type:String
    },
    transaction:Object,
});

//{{url}}/reserva/search

module.exports = mongoose.model('Reserva', reservaSchema);
