const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let habitacionSchema = new Schema({
    idhotel: {
        type: Schema.ObjectId,
        ref: "Hotel",
        required: true
    },
    nombre: {
        type: String,
        required: true,
    },
    descripcion: {
        type: String,
        required: true,
    },
    max_children: {
        type: Number,
        required: true,
    },
    ocupacion_max: {
        type: Number,
        required: true
    },
    galeria: [{
        type: String
    }],
    precioBase: {
        type: Number,
        required: true,
    },
    num_total: {
        type: Number,
        required: true
    },
    num_reservadas: {
        type: Number,
        default: 0
    },
    ocupacion: {
        camas: [{
            cama: {
                type:Schema.ObjectId,
                ref: 'Cama'
            },
            cantidad: {
                type: Number,
                default: 1
            }
        }],
        tarifas: [{
            temporada: {
                type: Schema.ObjectId,
                ref: 'Temporada'
            },
            precio:{
                type: Number
            }
        }],
        revenues: [{
            revenue:{
                type: Schema.ObjectId,
                ref: 'Temporada'
            },
            precio: {
                type: Number
            }
        }]
    }
});

habitacionSchema.methods.toJSON = function(){
    let habitacion = this;
    let habitacionObject = habitacion.toObject();

    let aux = [];
    for (let foto of habitacionObject.galeria) {
        aux.push(`/uploads/habtitacion/${foto}`);
    }

    habitacionObject.galeria = aux;
    
    return habitacionObject;
}

module.exports = mongoose.model('Habitacion', habitacionSchema);
