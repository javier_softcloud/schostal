const express = require('express');
const app = express();

//Importaciones de las rutas
const usuario = require('./usuario');
const admin = require('./admin');
const cama = require('./cama');
const activo = require('./activo');
const habitacion = require('./habitacion');
const hotel = require('./hotel');
const temporada = require('./temporada');
const promocion = require('./promocion');
const log = require('./log');
const consumible = require('./consumible');
const recepcionista = require('./recepcionista');
const upload = require('./upload');
const reserva = require('./reserva');

//Para formilario de contacto
app.use(require('./contacto'));
app.use('/cliente',usuario);
app.use('/admin',admin);
app.use('/cama', cama);
app.use('/activo', activo);
app.use('/hotel', hotel);
app.use('/habitacion', habitacion);
app.use('/temporada', temporada);
app.use('/promocion', promocion)
app.use('/log', log);
app.use('/reserva', reserva);
app.use('/consumible', consumible);
app.use('/recepcionista', recepcionista);
app.use('/upload', upload);

module.exports = app;
