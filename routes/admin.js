//Paquetes de node
const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const _ = require('underscore');
const speakeasy = require('speakeasy');
const { check, validationResult, matchedData } = require('express-validator');
//Importaciones locales
const log = require('../services/apilogger');
const Admin = require('../models/admin');
const {verificaToken,verificaTokenRoot} = require('../middlewares/auth');
const app = express();


//Obtener admin
app.get('/all', [verificaToken, verificaTokenRoot] , async (req,res) => {

  try {

    log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

    let admins = await Admin.find({ role: 'ADMIN_ROLE' }).populate('idhotel', 'nombre').exec();

    res.json({
      ok: true,
      admins
    });

  } catch (err) {
    log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
    return res.status(500).json({
      ok: false,
      err: { code: 400, msg: "Operation Failed", details: err }
    });
  }
});

//Agregar admin
app.post('/add', [check('nombre').exists().withMessage('Nombre required'),
  check('email')
    .exists()
    .withMessage('Email required')
    .isEmail().withMessage('Email not valid'),
  check('idhotel')
    .exists()
    .withMessage('idhotel required'),
  check('apPaterno')
    .exists()
    .withMessage('apPaterno required'),
  check('apMaterno')
    .exists()
    .withMessage('apMaterno required'),
  check('password')
    .exists()
    .withMessage('password required'), verificaToken, verificaTokenRoot] , async (req,res) => {

    try {

      log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

      let errors = validationResult(req);
      if (!errors.isEmpty()) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.body.email}","error":"${errors.mapped()}"}`);
        return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
      }

      let admin = new Admin({
        nombre: req.body.nombre,
        email: req.body.email,
        idhotel: req.body.idhotel,
        apPaterno: req.body.apPaterno,
        apMaterno: req.body.apMaterno,
        password: bcrypt.hashSync(req.body.password),
        accessHoteles: req.body.accessHoteles,
        accessHabitaciones: req.body.accessHabitaciones,
        accessAdministradores: req.body.accessAdministradores,
        accessReservaciones: req.body.accessReservaciones,
        accessTemporadas: req.body.accessTemporadas,
        accessClientes: req.body.accessClientes,
        accessCamas: req.body.accessCamas,
          accessContactos: req.body.accessContactos

      });

      let res1 = await admin.save();

      res.status(200).json({
        ok: true
      });

    } catch (err) {
      log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
      return res.status(500).json({
        ok: false,
        err: { code: 400, msg: "Operation Failed", details: err }
      });
    }
});


//Editicion de admin
app.put('/update/:id', [check('id').exists().withMessage('id required'), verificaToken, verificaTokenRoot], async (req,res) => {

  try {

    log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

    let errors = validationResult(req);
    if (!errors.isEmpty()) {
      log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
      return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
    }

    let _id = req.params.id;
    let body = _.pick(req.body, ["nombre", "email", "apPaterno", "apMaterno", "password",
               "accessHoteles", "accessHabitaciones", "accessAdministradores", "accessReservaciones", "accessTemporadas", "accessClientes", "accessCamas","accessContactos"]);
    console.log(body)

    let res1 = await Admin.findOneAndUpdate({_id}, body, { new: true, runValidators: true });

    res.json({
      ok: true,
        cliente: res1
    });

  } catch (error) {
    log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${error}"}`);
    return res.status(500).json({
      ok: false,
      err: { code: 400, msg: "Operation Failed", details: err }
    });
  }
});

//Borrar usuario
app.delete('/delete/:id', [check('id').exists().withMessage('id required'), verificaToken, verificaTokenRoot], async (req,res) => {

  try {

    log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

    let errors = validationResult(req);
    if (!errors.isEmpty()) {
      log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
      return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
    }

    let _id = req.params.id;

    let adminX = await Admin.findOneAndRemove({ _id});

    if (!adminX) {
      log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Admin not found"}`);
      return res.status(500).json({
        ok: false,
        err: { code: 400, msg: "Operation Failed", details: "Admin not found" }
      });
    }

    res.status(200).json({
      ok: true
    });

  } catch (err) {
    log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
    return res.status(500).json({
      ok: false,
      err: { code: 400, msg: "Operation Failed", details: err }
    });
  }
});

//Login normal
app.post('/login', [check('email').exists().withMessage('Email required'),
  check('password').exists().withMessage('Password required')], async (req,res) => {

  try {

    log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.body.email}"}`);

    let errors = validationResult(req);
    if (!errors.isEmpty()) {
      log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.body.email}","error":"${errors.mapped()}"}`);
      return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
    }

    let email = req.body.email;
    let pass = req.body.password;

    let adminDB = await Admin.findOne({ email });

    if (!adminDB) {
      log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.body.email}", "error":"Admin not found"}`);
      return res.status(500).json({
        ok: false,
        err: { code: 400, msg: "Operation Failed", details: "Admin not found" }
      });
    }

    console.log(bcrypt.hashSync(pass)," ",adminDB.password," ",!bcrypt.compareSync(pass, adminDB.password))

    if (!bcrypt.compareSync(pass, adminDB.password)) {
      log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.body.email}", "error":"Invalid Password"}`);
      return res.status(500).json({
        ok: false,
        err: { code: 400, msg: "Operation Failed", details: "Invalid Password" }
      });
    }

    let token = jwt.sign({
      admin: adminDB
    }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN });

    res.status(200).json({
      ok: true,
      admin: adminDB,
      token
    });

  } catch (err) {
    log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.body.email}", "error":"${err}"}`);
    return res.status(500).json({
      ok: false,
      err: { code: 400, msg: "Operation Failed", details: err }
    });
  }
});

//Login root
app.post('/root', [check('token').exists().withMessage('Token required')], async (req,res) => {

  try {

    log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "token":"${req.body.token}"}`);

    let errors = validationResult(req);
    if (!errors.isEmpty()) {
      log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","token":"${req.body.token}","error":"${errors.mapped()}"}`);
      return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
    }

    let token = req.body.token;

    let verified = speakeasy.totp.verify({
      secret: process.env.SECRET,
      encoding: 'base32',
      token
    });

    if (!verified) {
      log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","token":"${req.body.token}", "error":"Invalid Token"}`);
      return res.status(500).json({
        ok: false,
        err: { code: 400, msg: "Operation Failed", details: "Invalid Token" }
      });
    }

    let adminDB = await Admin.findOne({ role: "ROOT" });

    if (!adminDB) {
      log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","token":"${req.body.token}", "error":"ROOT doesnt exists"}`);
      return res.status(500).json({
        ok: false,
        err: { code: 400, msg: "Operation Failed", details: "ROOT doesnt exists" }
      });
    }

    let token_jwt = jwt.sign({
      admin: adminDB
    }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN });

    res.status(200).json({
      ok: true,
      admin: adminDB,
      token: token_jwt
    });

  } catch (err) {
    log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","token":"${req.body.token}", "error":"${err}"}`);
    return res.status(500).json({
      ok: false,
      err: { code: 400, msg: "Operation Failed", details: err }
    });
  }
});


module.exports = app;
