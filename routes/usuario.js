//Paquetes de node
const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const _ = require('underscore');
//Para google
const {OAuth2Client} = require('google-auth-library');
const client =  new OAuth2Client(process.env.CLIENT_ID);
//Importaciones locales
const Usuario = require('../models/usuario');
const { check, validationResult, matchedData } = require('express-validator');
const {verificaToken, verificaTokenAdmin, verificaTokenRoot} = require('../middlewares/auth');
const Openpay = require('openpay');
const openpay = new Openpay('mvapdjupuasvisu21wzg','sk_c2b834765fcc4d858e3f37c23f535c6b');
const log = require('../services/apilogger');
const app = express();

//Obtener usuarios
app.get('/all', [verificaToken, verificaTokenAdmin], async (req,res) => {

  try {

    log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

    let usuarios = await Usuario.find({});
    res.json({
      ok: true,
      usuarios
    });

  } catch (err) {
    log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
    return res.status(500).json({
      ok: false,
      err: { code: 400, msg: "Operation Failed", details: err }
    });
  }
});

app.get('/me', [verificaToken], async (req,res) => {

  try {

    log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "user":"${req.usuario._id}"}`);

    let id = req.usuario._id;

    let user = await Usuario.findOne({ _id: id }).exec();

    if (!user) {
      log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","user":"${req.usuario._id}", "error":"El id no existe"}`);
      return res.status(500).json({
        ok: false,
        err: { code: 400, msg: "Operation Failed", details: 'El id no existe' }
      });
    }

    res.json({
      ok: true,
      usuario: user
    });

  } catch (err) {
    log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","user":"${req.usuario._id}", "error":"${err}"}`);
    return res.status(500).json({
      ok: false,
      err: { code: 400, msg: "Operation Failed", details: err }
    });
  }
});

//Agregar usuario
app.post('/add', [check('nombre')
    .exists().withMessage('Nombre is required'),
  check('apPaterno')
    .exists().withMessage('apPaterno is required'),
  check('apMaterno')
    .exists().withMessage('apMaterno is required'),
  check('email')
    .exists().withMessage('Email is required')
    .isEmail().withMessage('Incorrect value for email'),
  check('password')
    .exists().withMessage('Password is required')], async (req,res) => {

    try {

      log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "email":"${req.body.email}"}`);

      let errors = validationResult(req);
      if (!errors.isEmpty()) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.body.email}","error":"${errors.mapped()}"}`);
        return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
      }

      let openpayClient = {
        'name': req.body.nombre,
        "last_name": req.body.apPaterno + " " + req.body.apMaterno,
        'email': req.body.email,
        'requires_account': true
      }

      openpay.customers.create(openpayClient, async (error, customer) => {

        if (error) {
          log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.body.email}", "error":"${error}"}`);
          return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: `${error}` }
          });
        }

        let usuario = new Usuario({
          nombre: req.body.nombre,
          apPaterno: req.body.apPaterno,
          apMaterno: req.body.apMaterno,
          email: req.body.email,
          password: bcrypt.hashSync(req.body.password, 10),
          idOpenpay: customer.id
        });

        let user = await usuario.save();

        let token = jwt.sign({
          usuario: usuarioDB
        }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN });


        res.json({
          ok: true,
          usuario: user,
          token
        });

      });
    } catch (err) {
      log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"Not Logged", "error":"${err}"}`);
      return res.status(500).json({
        ok: false,
        err: { code: 400, msg: "Operation Failed", details: err }
      });
    }
});

//Editicion del usuario
app.put('/update/:id', [check('id').exists().withMessage('id required'), verificaToken], async (req,res) => {

  try {

    log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "usuario":"${req.usuario._id}"}`);

    let errors = validationResult(req);
    if (!errors.isEmpty()) {
      log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
      return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
    }

    let id = req.params.id;
    let body = _.pick(req.body, ["nombre", "fechaNacimiento", "apPaterno", "apMaterno", "telefono"]);

    let user = await Usuario.findOneAndUpdate({ _id: id }, body, { new: true, runValidators: true });

    res.json({
      ok: true,
      usuario: user
    });
  } catch (err) {
    log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","usuario":"${req.usuario._id}", "error":"${err}"}`);
    return res.status(500).json({
      ok: false,
      err: { code: 400, msg: "Operation Failed", details: err }
    });
  }
});

//Borrar usuario
app.delete('/delete/:id', [check('id').exists().withMessage('id required'), verificaToken, verificaTokenRoot], async (req,res) => {

  try {

    log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

    let errors = validationResult(req);
    if (!errors.isEmpty()) {
      log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
      return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
    }

    let id = req.params.id;

    let res1 = await Usuario.findOneAndRemove({ _id: id });

    res.json({
      ok: true
    });

  } catch (err) {
    log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
    return res.status(500).json({
      ok: false,
      err: { code: 400, msg: "Operation Failed", details: err }
    });
  }
});

//Login normal
app.post('/login', [check('email').exists().withMessage('Email required'),
  check('password')
  .exists()
  .withMessage('Password required')], async (req,res) => {

    try {

      log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "usuario":"${req.body.email}"}`);

      let email = req.body.email;
      let pass = req.body.password;

      let user = await Usuario.findOne({ email });

      if (!user) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","usuario":"${req.body.email}", "error":"User not found"}`);
        return res.status(500).json({
          ok: false,
          err: { code: 400, msg: "Operation Failed", details: 'User not found' }
        });
      }

      if (!bcrypt.compareSync(pass, user.password)) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","usuario":"${req.body.email}", "error":"Invalid password"}`);
        return res.status(500).json({
          ok: false,
          err: { code: 400, msg: "Operation Failed", details: 'Invalid password' }
        });
      }

      let token = jwt.sign({
        usuario: user
      }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN });

      res.json({
        ok: true,
        usuario: user,
        token
      });

    } catch (err) {
      log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","usuario":"${req.body.email}", "error":"${err}"}`);
      return res.status(500).json({
        ok: false,
        err: { code: 400, msg: "Operation Failed", details: err }
      });
    }
});

app.post('/google', async(req,res) => {

  let googleUser = await verify(token)
                    .catch(e=>{
                      return res.status(403).json({
                        ok:false,
                        err:e
                      });
                    });

  Usuario.findOne({email: googleUser.email}, (err,usuarioDB) => {
      if(err){
        return res.status(500).json({
          ok:false,
          err
        });
      }

      if(usuarioDB){
        if(usuarioDB.google === false){
          return res.status(400).json({
            ok:false,
            err: "Debe de usar su auth normal"
          });
        }
        else{
          let token = jwt.sign({
            usuario:usuarioDB,
          },process.env.SEED, {expiresIn:process.env.CADUCIDAD_TOKEN});

          return res.json({
            ok:true,
            usuario: usuarioDB,
            token
          });
        }
      }
      else{
        let usuario = new Usuario();

        usuario.nombre = googleUser.nombre;
        usuario.email = googleUser.email;
        usuario.img = googleUser.img;
        usuario.google = true;
        usuario.password = ':)';

        usuario.save((err, usuarioDB) => {

          if(err){
            return res.status(500).json({
              ok:false,
              err
            });
          }

          let token = jwt.sign({
            usuario:usuarioDB,
          },process.env.SEED, {expiresIn:process.env.CADUCIDAD_TOKEN});

          res.json({
            ok:true,
            usuario: usuarioDB,
            token
          });

        });
      }
    });
});

//Confguraciones de google
async function verify(token) {
  const ticket = await client.verifyIdToken({
      idToken: token,
      audience: process.env.CLIENT_ID
  });
  const payload = ticket.getPayload();

  return {
    nombre: payload.name,
    email: payload.email,
    img: payload.picture,
    google: true
  }
}

module.exports = app;
