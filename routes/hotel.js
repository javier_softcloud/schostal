//PAquetes de node
const express =  require('express');
const _ = require('underscore');
const mongoose = require('mongoose');
const { check, validationResult, matchedData } = require('express-validator');
//Importaciones locales
const Hotel = require('../models/hotel');
const Cama = require('../models/cama');
// const Log = require('../models/log');
const log = require('../services/apilogger');
const Activo = require('../models/activo');
const Promocion = require('../models/promocion');
const Temporada = require('../models/temporada');
const { verificaToken, verificaTokenRoot, verificaTokenAdmin } = require('../middlewares/auth');

const app = express();

app.post('/add', [check('nombre').exists().withMessage('Nombre is required'),
    check('direccion').exists().withMessage('Direccion is required'),
    check('tipo').exists().withMessage('Tipo is required'),
    check('politicas_ninios').exists().withMessage('Politicas_ninios is required'),
    check('max_age_children').exists().withMessage('Mx_age_children is required'),
    verificaToken, verificaTokenRoot], async (req,res) => {
        
    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);
        
        let hotel = new Hotel({
            nombre: req.body.nombre,
            direccion: req.body.direccion,
            tipo: req.body.tipo,
            politicas_ninios: req.body.politicas_ninios,
            max_age_children: req.body.max_age_children,
            servicios: req.body.servicios,
            galeria: req.body.galeria,
            checkin: req.body.checkin,
            checkout: req.body.checkout
        });

        let res1 = await hotel.save();

        res.status(200).json({
            ok: true,
            msg: "Operacion completada"
        });
        
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }    
});

app.post('/addservicio/:id', [check('id').exists().withMessage('id required'), verificaToken, verificaTokenAdmin], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let _id = req.body.params;

        let res1 = await Hotel.findOneAndUpdate( { _id }, { $push: { servicios: req.body.servicio } });

        res.status(200).json({
            ok: true,
            msg: "Operacion exitosa"
        });
        
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

app.get('/all', [verificaToken, verificaTokenRoot], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);
        
        let hoteles = await Hotel.find({}).exec();
        
        res.status(200).json({
            ok: true,
            hoteles
        });
    } catch (error) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

app.get('/public', (req,res) => {
    Hotel.find({}).exec((err, hoteles) => {
        if (err != null) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        res.status(200).json({
            ok: true,
            hoteles
        });
    });
});

app.put('/update/:id', [check('id').exists().withMessage('id required'), verificaToken, verificaTokenAdmin], async (req,res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }
        
        let _id = req.params.id;
        let body = _.pick(req.body, ["nombre", "direccion", "tipo", "politicas_ninios", "max_age_children"]);

        let res1 = await Hotel.findOneAndUpdate( { _id }, body, { new: true, runValidators: true });

        res.status(200).json({
            ok: true
        });

    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
}); 

app.delete('/delete/:id', [check('id').exists().withMessage('id required'), verificaToken, verificaTokenRoot], async (req,res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let id = req.params.id;

        let camas = Cama.find({ idhotel: id }).exec();

        if (camas > 0) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Camas existentes dentro del hotel"}`);
            return res.status(500).json({
                ok: false,
                err: { code: 400, msg: "Operation Failed", details: "Camas existentes dentro del hotel" }
            });
        }

        let activos = Activo.find({ idhotel: id }).exec();

        if (activos > 0) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Activos existentes dentro del hotel"}`);
            return res.status(500).json({
                ok: false,
                err: { code: 400, msg: "Operation Failed", details: "Activos existentes dentro del hotel" }
            });
        }

        let temporadas = Temporada.find({ idhotel: id }).exec();

        if (temporadas > 0) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Temporadas existentes dentro del hotel"}`);
            return res.status(500).json({
                ok: false,
                err: { code: 400, msg: "Operation Failed", details: "Temporadas existentes dentro del hotel" }
            });
        }

        let promociones = Promocion.find({ idhotel: id }).exec();

        if (promociones > 0) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Promociones existentes dentro del hotel"}`);
            return res.status(500).json({
                ok: false,
                err: { code: 400, msg: "Operation Failed", details: "Promociones existentes dentro del hotel" }
            });
        }

        let res1 = Hotel.findOneAndRemove({ _id: id });

        res.status(200).json({
            ok: true
        });
        
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
    /* let id = req.params.id;
    Cama.find({ idhotel: id }).exec((err,camas) => {
        if(err != null){
            return res.status(500).json({
                ok: false,
                err: "Hotel no encontrada"
            });
        }
        if(camas.length>0){
            return res.status(500).json({
                ok: false,
                err: "Camas existentes dentro de hotel"
            });
        }
    });
    Activo.find({ idhotel: id }).exec((err, activos) => {
        if (err != null) {
            return res.status(500).json({
                ok: false,
                err: "Hotel no encontrada"
            });
        }
        if (activos.length > 0) {
            return res.status(500).json({
                ok: false,
                err: "Activos existentes dentro de hotel"
            });
        }
    });
    Temporada.find({ idhotel: id }).exec((err, temporadas) => {
        if (err != null) {
            return res.status(500).json({
                ok: false,
                err: "Hotel no encontrada"
            });
        }
        if (temporadas.length > 0) {
            return res.status(500).json({
                ok: false,
                err: "Temporadas existentes dentro de hotel"
            });
        }
    });
    Promocion.find({ idhotel: id }).exec((err, promociones) => {
        if (err != null) {
            return res.status(500).json({
                ok: false,
                err: "Hotel no encontrada"
            });
        }
        if (promociones.length > 0) {
            return res.status(500).json({
                ok: false,
                err: "Promocioones existentes dentro de hotel"
            });
        }
    });
    Hotel.findOneAndRemove({ _id: id }, (err, hotel) => {
        if (err != null) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!hotel) {
            return res.status(500).json({
                ok: false,
                err: "Hotel no encontrada"
            });
        }
        // Borra todo lo que tenga ese id
        Cama.removeMany({idhotel:id}, (err) => {
            if (err != null) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
        });
        Log.remove({ idhotel: id }, (err) => {
            if (err != null) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
        });
        Activo.removeMany({ idhotel: id }, (err) => {
            if (err != null) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
        });
        Temporada.removeMany({ idhotel: id }, (err) => {
            if (err != null) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
        });
        Promocion.removeMany({ idhotel: id }, (err) => {
            if (err != null) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
        });

        res.status(200).json({
            ok: true
        });
    }); */
});

module.exports = app;
