//Paquetes de node
const express = require('express');
const _ = require('underscore');
const { check, validationResult, matchedData } = require('express-validator');
//Importaciones locales
const Cama = require('../models/cama');
const Log = require('../models/log');
const log = require('../services/apilogger');
const { verificaToken, verificaTokenAdmin, verificaAdminHotel } = require('../middlewares/auth');

const app = express();

//Agregar una cama
app.post('/add',[check('nombre').exists().withMessage('nombre required'), 
    check('ocupacion').exists().withMessage('ocupacion required'),
    check('idhotel').exists().withMessage('idhotel required'), 
    check('tipo').exists().withMessage('tipo required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req,res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let cama = new Cama({
            nombre: req.body.nombre,
            ocupacion: req.body.ocupacion,
            idhotel: req.body.idhotel,
            tipo: req.body.tipo,
            total: req.body.total
        });

        let camaDB = await cama.save();
        let log_bien = await guardaLog(req.body.idhotel, "Cama agregada", req.admin._id);

        res.json({
            ok: true,
            cama: camaDB
        });

    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

//Obtener todas las camas
app.get('/all:id*?', [verificaToken, verificaTokenAdmin] , async (req,res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);
        
        let idhotel = req.admin.idhotel;

        if (req.admin.role == 'ROOT') {
            idhotel = req.params.id;
            if (!idhotel) {
                let camas = await Cama.find({}).exec();
                res.json({
                    ok: true,
                    camas
                });
            } else {
                let camas = await Cama.find({ idhotel }).exec();
                res.json({
                    ok: true,
                    camas
                });
            }
        } else {
            if (!idhotel) {
                log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Missing idhotel"}`);
                return res.status(500).json({
                    ok: false,
                    err: { code: 400, msg: "Operation Failed", details: "Missing idhotel" }
                });
            }
            let camas = await Cama.find({ idhotel }).exec();
            res.json({
                ok: true,
                camas
            });
        }
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

//Editar cama
app.put('/update/:id', [check('id').exists().withMessage('id required'), 
    check('idhotel').exists().withMessage('idhotel required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req,res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let _id = req.params.id;
        let body = _.pick(req.body, ["nombre", "ocupacion", "tipo", "total", "ocupadas", "asignadas"]);

        let cama = await Cama.findOneAndUpdate({ _id, idhotel: req.body.idhotel }, body, { new: true });
        let log_bien = await guardaLog(req.body.idhotel, "Cama editada", req.admin._id);

        res.json({
            ok: true,
            cama
        });        
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

//Borra cama
app.delete('/delete/:idhotel/:id', [check('id').exists().withMessage('id required'), 
    check('idhotel').exists().withMessage('idhotel required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req,res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let id = req.params.id;
        let idhotel = req.params.idhotel;

        let cama = await Cama.findByIdAndRemove({ _id: id, idhotel });
        let log_bien = await guardaLog(idhotel, "Cama borrada", req.admin._id);

        res.json({
            ok: true
        });
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

function guardaLog(idhotel, actividad, idadmin) {
    return new Promise(resolve => {
        let fecha = new Date();
        let log = new Log({
            idhotel,
            actividad,
            idadmin,
            fecha
        });

        log.save((err) => {
            if (err != null) {
                resolve(false);
            }

            resolve(true);
        });
    });
}


module.exports = app;