//Paquetes de node
const express = require('express');
//Importaciones locales
const Log = require('../models/log');
const {verificaToken, verificaTokenAdmin, verificaAdminHotel} = require('../middlewares/auth');

const app = express();

app.get('/all', [verificaToken, verificaTokenAdmin, verificaAdminHotel],(req,res) => {
    let idhotel = req.body.idhotel;
    let fecha1 = req.body.fecha1;
    let fecha2 = req.body.fecha2;

    if(req.admin.role == 'ROOT'){
        if(!idhotel){
            Log.find({}).populate('idadmin','nombre role').exec((err, logs) => {
                if(err != null){
                    return res.status(500).json({
                        ok:false,
                        err
                    });
                }

                return res.json({
                    ok:true,
                    logs
                });
            });
        }
        else{
            Log.find({ idhotel }).populate('idadmin','nombre role').exec((err, logs) => {
                if (err != null) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                return res.json({
                    ok: true,
                    logs
                });
            });
        }
    }
    else{
        Log.find({ idhotel }).populate('idadmin','nombre role').exec((err, logs) => {
            if (err != null) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }

            return res.json({
                ok: true,
                logs
            });
        });
    }
});

module.exports = app;
