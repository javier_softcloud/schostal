//Paquetes de node
const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { check, validationResult, matchedData } = require('express-validator');
const _ = require('underscore');
const speakeasy = require('speakeasy');
//Importaciones locales
const Admin = require('../models/admin');
const { verificaToken, verificaTokenAdmin, verificaAdminHotel } = require('../middlewares/auth');
const app = express();


//Obtener admin
app.get('/all/:id*?', [verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);
        
        let idhotel = req.admin.idhotel;

        if (req.admin.role == 'ROOT'){
            idhotel = req.params.id;

            if (!idhotel) {
                let receps = await Admin.find({ role: 'RECEP_ROLE' }).populate('idhotel', 'nombre').exec();
                res.status(200).json({
                    ok: true,
                    receps
                });
            } else {
                let receps = await Admin.find({ idhotel, role: 'RECEP_ROLE' }).populate('idhotel', 'nombre').exec();
                res.status(200).json({
                    ok: true,
                    receps
                });
            }
        } else {
            if (!idhotel) {
                log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Missing idhotel"}`);
                return res.status(500).json({
                    ok: false,
                    err: { code: 400, msg: "Operation Failed", details: "Missing idhotel" }
                });
            }
            let receps = Admin.find({ idhotel, role: 'RECEP_ROLE' }).populate('idhotel', 'nombre').exec();
            res.status(200).json({
                ok: true,
                receps
            });
        }
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

//Agregar recepcionista
app.post('/add', [check('nombre').exists().withMessage('Nombre is required'),
    check('email').exists().withMessage('Email is required'),
    check('idhotel').exists().withMessage('Hotel is required'),
    check('apPaterno').exists().withMessage('apPaterno is required'),
    check('apMaterno').exists().withMessage('apMaterno is required'),
    check('password').exists().withMessage('Password is required'), 
    verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let admin = new Admin({
            nombre: req.body.nombre,
            email: req.body.email,
            idhotel: req.body.idhotel,
            apPaterno: req.body.apPaterno,
            apMaterno: req.body.apMaterno,
            recep: req.body.recep,
            mainHotel: req.body.mainHotel,
            pHoteles: req.body.pHoteles,
            habs: req.body.habs,
            admin: req.body.admin,
            reservas: req.body.reservas,
            tarifas: req.body.tarifas,
            pHoteles: req.body.tarifas,
            users: req.body.users,
            camas: req.body.camas,
            role: 'RECEP_ROLE',
            password: bcrypt.hashSync(req.body.password)
        });

        let res1 = await admin.save();

        res.status(200).json({
            ok: true
        });        
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

//Editicion de recepcionista
app.put('/update/:id', [check('id').exists().withMessage('id required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let id = req.params.id;
        let body = _.pick(req.body, ["nombre", "email", "apPaterno", "apMaterno", "password", "recep",
            "mainHotel", "pHoteles", "habs", "admin", "reservas", "tarifas", "pHoteles", "users", "camas"]);

        let res1 = await Admin.findOneAndUpdate(id, body, { new: true, runValidators: true });

        res.json({
            ok: true
        });        
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

//Borrar recepcionista
app.delete('/delete/:id', [check('id').exists().withMessage('id required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }
        
        let id = req.params.id;

        let res1 = await Admin.findOneAndRemove({ _id: id });

        res.status(200).json({
            ok: true
        });        
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

//Login normal
app.post('/login', [check('email').exists().withMessage('Email required'),
    check('password').exists().withMessage('Password required')], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.body.email}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.body.email}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let email = req.body.email;
        let pass = req.body.password;

        let adminDB = await Admin.findOne({ email, role: 'RECEP_ROLE' });
        
        if (!adminDB) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.body.email}", "error":"Admin not found"}`);
            return res.status(500).json({
                ok: false,
                err: { code: 400, msg: "Operation Failed", details: "Admin not found" }
            });
        }

        if (!bcrypt.compareSync(pass, adminDB.password)) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.body.email}", "error":"Invalid Password"}`);
            return res.status(500).json({
                ok: false,
                err: { code: 400, msg: "Operation Failed", details: "Invalid Password" }
            });
        }

        let token = jwt.sign({
            admin: adminDB
        }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN });

        res.status(200).json({
            ok: true,
            admin: adminDB,
            token
        });
        
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.body.email}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});


module.exports = app;
