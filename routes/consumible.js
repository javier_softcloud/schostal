//Paquetes de node
const express = require('express');
const _ = require('underscore');
const { check, validationResult, matchedData } = require('express-validator');
//Importaciones locales
const Consumible = require('../models/consumible');
const Log = require('../models/log');
const log = require('../services/apilogger');
const { verificaToken, verificaTokenAdmin, verificaAdminHotel, verificaTokenRoot } = require('../middlewares/auth');

const app = express();

//Agregar una consumible
app.post('/add', [check('idhotel').exists().withMessage('idhotel required'),
    check('nombre').exists().withMessage('nombre required'),
    check('descripcion').exists().withMessage('descripcion required'),
    check('cantidad').exists().withMessage('cantidad required'),
    check('precio').exists().withMessage('precio required'), 
    verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let consumible = new Consumible({
            idhotel: req.body.idhotel,
            nombre: req.body.nombre,
            descripcion: req.body.descripcion,
            precio: req.body.precio
        });

        let res1 = await consumible.save();
        let log_bien = await guardaLog(req.body.idhotel, "Consumible agregado", req.admin._id);

        res.status(200).json({
            ok: true
        });
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

//Obtener todas las consumibles
app.get('/all/:id*?', [verificaToken, verificaTokenAdmin], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let idhotel = req.admin.idhotel;

        if (req.admin.role == 'ROOT') {
            idhotel = req.params.id;
            if (!idhotel) {
                let consumibles = await Consumible.find({}).exec();
                res.status(200).json({
                    ok: true,
                    consumibles
                });
            } else {
                let consumibles = await Consumible.find({ idhotel }).exec();
                res.status(200).json({
                    ok: true,
                    consumibles
                });
            }
        } else {
            if (!idhotel) {
                log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Missing idhotel"}`);
                return res.status(500).json({
                    ok: false,
                    err: { code: 400, msg: "Operation Failed", details: "Missing idhotel" }
                });
            }
            let consumibles = await Consumible.find({ idhotel }).exec();
            res.status(200).json({
                ok: true,
                consumibles
            });
        }
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

//Editar consumible
app.put('/update/:id', [check('id').exists().withMessage('id required'),
    check('idhotel').exists().withMessage('idhotel required'),
    verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {
    
    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let id = req.params.id;
        let idhotel = req.body.idhotel;
        let body = _.pick(req.body, ["nombre", "precio", "descripcion", "cantidad"]);

        let consumible = await Consumible.findOneAndUpdate({ _id: id, idhotel }, body, { new: true });
        let log_bien = await guardaLog(idhotel, "Consumible editado", req.admin._id);

        res.status(200).json({
            ok: true,
            consumible
        });
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

//Borra consumible
app.delete('/delete/:idhotel/:id', [check('id').exists().withMessage('id required'),
    check('idhotel').exists().withMessage('idhotel required'),
    verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {
    
    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let res1 = await Consumible.findByIdAndRemove({ _id: id, idhotel });
        let log_bien = await guardaLog(idhotel, "Consumible eliminado", req.admin._id);

        res.status(200).json({
            ok: true
        });
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

function guardaLog(idhotel, actividad, idadmin) {
    return new Promise(resolve => {
        let fecha = new Date();
        let log = new Log({
            idhotel,
            actividad,
            idadmin,
            fecha
        });

        log.save((err) => {
            if (err != null) {
                resolve(false);
            }

            resolve(true);
        });
    });
}


module.exports = app;