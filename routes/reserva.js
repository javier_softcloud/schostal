//Paquetes de node
const _ = require('underscore');
const express = require('express');
//Importaciones locales
const Habitacion = require('../models/habitacion');
const Hotel = require('../models/hotel');
const Reserva = require('../models/reserva');
const { verificaToken, verificaTokenAdmin, verificaAdminHotel } = require('../middlewares/auth');
const Openpay = require('openpay');
const openpay = new Openpay('mvapdjupuasvisu21wzg','sk_c2b834765fcc4d858e3f37c23f535c6b');

const app = express();

const DAYS = 86400000;

app.post('/search', async (req,res) => {
    let idhotel = req.body.idhotel;
    let checkin = new Date(req.body.checkin);
    let checkout = new Date(req.body.checkout);
    let habs = getQueryHabs(req.body.habs);

    let opciones = [];

    for(let hab of habs){
        let op = {
            ocup: hab.ocup,
            ops: await getReservationOptions(idhotel, hab, checkin, checkout)
        }
        opciones.push(op);
    }

    Hotel.findOne({_id:idhotel}).exec((err2, hotel) => {

        if (err2 != null) {
            return res.status(500).json({
                ok: false,
                err2
            });
        }

        if (opciones.length <= 0) {
            res.status(200).json({
                info: _.pick(hotel,["_id","servicios","galeria","nombre","direccion","politicas_ninios"]),
                ok: false,
                msg: "No hay disponibilidad"
            });
        }
        else {
            res.status(200).json({
                info: _.pick(hotel, ["_id","servicios", "galeria", "nombre", "direccion", "politicas_ninios"]),
                ok: true,
                opciones
            });
        }
    });
});

app.post('/confirm', [verificaToken], (req,res) => {
    let transaction;
    let idhotel = req.body.idhotel;
    let habs = req.body.habs;
    let ocupacionAdultos = req.body.ocupacionAdultos;
    let ocupacionNinos = req.body.ocupacionNinos;
    let total = req.body.total;
    let idusuario = req.body.idusuario;
    let checkin = new Date(req.body.checkin);
    let checkout = new Date(req.body.checkout);
    let creditCard = req.body.creditCard;


    var chargeRequest = {
        'source_id' : creditCard,
        'method' : 'card',
        'amount' : total,
        'cvv2' : req.body.cvc,
        'currency' : 'MXN',
        'description' : 'Reserva de Habitación',
        'device_session_id' : "sadasdasdmwlkdeiudi38297843"
    }

    console.log(chargeRequest)



    // Array nuevo
    let guests = req.body.guests;



    openpay.customers.charges.create(req.usuario.idOpenpay,chargeRequest, function(error, transaction) {
        if (error)
            return res.status(500).json({
                ok: false,
                error
            });

        console.log("Transaction",transaction)

        let reservacion = new Reserva({
            idhotel:idhotel,
            habs:habs,
            status:"confirmada",
            ocupacionAdultos:ocupacionAdultos,
            ocupacionNinos:ocupacionNinos,
            total:total,
            idusuario:idusuario,
            checkin:checkin,
            guests,
            checkout:checkout,
            transaction: transaction,
            creditCard: creditCard
        });

        reservacion.save((err2,reservation) => {
            if (err2 != null) {
                return res.status(500).json({
                    ok: false,
                    err:err2
                });
            }



            Habitacion.updateMany({_id: { $in:habs }},{reservada:true},(err) => {
                if(err != null){
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                return res.status(200).json({
                    ok: true,
                    reservation
                });
            });

        });
    });
});

app.post('/add/card', [verificaToken], (req,res) => {
    let card = req.body;
    let idOpenpay = req.usuario.idOpenpay;
    let expDate = card.expDate.split("/");
    let expMonth = expDate[0].replace(/ /g, "");
    let expYear = expDate[1].replace(/ /g, "");

    let cardRequest = {
        'card_number':card.ccNumber.replace(/ /g, ""),
        'holder_name':card.ccClientName,
        'expiration_year':expYear,
        'expiration_month':expMonth,
        'cvv2': card.cvcNumber,
        'device_session_id': req.get('Authorization')
    };

    console.log(cardRequest)

    openpay.customers.cards.create(idOpenpay, cardRequest, function(error, card)  {
        if(error)
            return res.status(500).json({
                ok:false,
                err:error
            })

        res.status(200).json(card);
    });

});

app.delete('/delete/:id', [verificaToken, verificaAdminHotel], (req,res) => {
    let id = req.params.id;

    Reserva.findOneAndRemove({_id:id}, (err) => {
        if (err != null) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        res.json({
            ok:true
        });
    });
});

app.get('/user', [verificaToken], (req,res) => {
    let idusuario;

    try {
        idusuario = req.usuario._id;
    } catch (error) {
        return res.status(500).json({
            ok:false,
            err:error
        })
    }

    Reserva.find({idusuario}, (err, reservaciones) => {
        if (err != null) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        res.status(200).json({
            ok: true,
            reservaciones
        });
    });
});


app.get('/ccard', [verificaToken], (req,res) => {
    let usuario = req.usuario;
    let searchParams = {
        'limit' : 10
    }

    openpay.customers.cards.list(usuario.idOpenpay, searchParams, function(error, list){
        if(error)
            return res.status(500).json({
                ok: false,
                err
            });

        res.status(200).json(list);
    });
});


app.get('/all/:id*?', [verificaToken, verificaTokenAdmin, verificaAdminHotel], (req,res) => {
    let idhotel = req.admin.idhotel;
    if (req.admin.role == 'ROOT') {
        idhotel = req.params.id;
        if (!idhotel) {
            Reserva.find({}).exec((err, reservaciones) => {
                if (err != null) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                res.status(200).json({
                    ok: true,
                    reservaciones
                });
            });
        }
        else {
            Reserva.find({ idhotel }).exec((err, reservaciones) => {
                if (err != null) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                res.status(200).json({
                    ok: true,
                    reservaciones
                });
            });
        }
    }
    else {
        Reserva.find({ idhotel }).exec((err, reservaciones) => {
            if (err != null) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }

            res.status(200).json({
                ok: true,
                reservaciones
            });
        });
    }
});

function getPrecio(base, tarifas, revs, fecha1, fecha2){
    return new Promise((resolve) => {
        let res = base;
        if(revs.length === 0){
            for (let tarifa of tarifas) {
                if (fecha1 >= tarifa.temporada.fechaInicio && fecha2 <= tarifa.temporada.fechaFinal) {
                    res = tarifa.precio;
                }
            }
        }
        else{
            for(let rev of revs ){
                if (fecha1 >= rev.revenue.fechaInicio && fecha2 <= rev.revenue.fechaFinal) {
                    res = tarifa.precio;
                }
            }
        }

        resolve(res);
    });
}

function getReservationOptions(idhotel, hab, checkin, checkout){
    return new Promise( (resolve) => {
        Habitacion.find({
            idhotel,
            ocupacion_max: { $eq: hab.ocup.adultos },
            max_children: { $gte: hab.ocup.ninos }
            }).populate('ocupacion.camas', 'nombre ocupacion')
            .populate('ocupacion.tarifas.temporada', 'fechaInicio fechaFinal')
            .populate('ocupacion.revenues.revenue', 'fechaInicio fechaFinal').exec(async (err, habsQuery) => {

                if (err != null) {
                    resolve('Mal');
                }

                let opciones = [];

                for(let hQuery of habsQuery){
                    let precio = await getPrecio(hQuery.precioBase, hQuery.ocupacion.tarifas, hQuery.ocupacion.revenues, checkin, checkout);
                    let opcion = hQuery.toObject();
                    delete opcion.ocupacion.tarifas;
                    delete opcion.ocupacion.revenues;
                    delete opcion.precioBase;
                    delete opcion.__v;
                    opcion['precio'] = precio;
                    opcion['cantidad'] = hab.cantidad;
                    opcion['total'] = precio*((checkout-checkin)/DAYS)
                    opciones.push(opcion);
                }


                resolve(opciones);

            });
    });
}

function getQueryHabs(habs) {
    let arr = [];

    for(let hab of habs){
        let struct = new Object();
        if(arr.length == 0){
            struct.ocup = hab;
            struct.cantidad = 1;
            arr.push(struct);
            continue;
        }
        let contained = false;
        for(let qa of arr) {
            if ((qa.ocup.adultos == hab.adultos) && (qa.ocup.ninos == hab.ninos)){
                qa.cantidad++;
                contained = true;
            }
        }
        if(!contained){
            struct.ocup = hab;
            struct.cantidad = 1;
            arr.push(struct);
        }
    }
    return arr;
}

module.exports = app;
