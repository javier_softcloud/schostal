//Paquetes de node
const express = require('express');
const _ = require('underscore');
const { check, validationResult, matchedData } = require('express-validator');
//Importaciones locales
const Temporada = require('../models/temporada');
const Log = require('../models/log');
const log = require('../services/apilogger');
const {verificaToken, verificaTokenAdmin, verificaAdminHotel} = require('../middlewares/auth');

const app = express();

//Agregar una temporada
app.post('/addtemporada', [
    check('nombre').exists().withMessage('nombre required'),
    check('fechaInicio').exists().withMessage('fechaInicio required'),
    check('fechaFinal').exists().withMessage('fechaFinal required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req,res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let temp = new Temporada({
            idhotel: req.body.idhotel,
            nombre: req.body.nombre,
            fechaInicio: new Date(req.body.fechaInicio),
            fechaFinal: new Date(req.body.fechaFinal)
        });

        let resT = await validaFechas(temp.idhotel, temp.fechaInicio, temp.fechaFinal, 'NORMAL');

        if (!resT) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Las fechas se traslapan"}`);
            return res.status(500).json({
                ok: false,
                err: { code: 400, msg: "Operation Failed", details: `Las fechas se traslapan` }
            });
        }

        let res1 = await temp.save();

        res.json({
            ok: true
        });        
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

//Agregar revenue
app.post('/addrevenue', [check('idhotel').exists().withMessage('idhotel required'),
    check('nombre').exists().withMessage('nombre required'),
    check('fechaInicio').exists().withMessage('fechaInicio required'),
    check('fechaFinal').exists().withMessage('fechaFinal required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async(req,res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let temp = new Temporada({
            idhotel: req.body.idhotel,
            nombre: req.body.nombre,
            tipo: 'REV',
            fechaInicio: new Date(req.body.fechaInicio),
            fechaFinal: new Date(req.body.fechaFinal)
        });

        let resT = await validaFechas(temp.idhotel, temp.fechaInicio, temp.fechaFinal, 'REV');

        if (!resT) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Las fechas se traslapan"}`);
            return res.status(500).json({
                ok: false,
                err: { code: 400, msg: "Operation Failed", details: `Las fechas se traslapan` }
            });
        }

        let res = await temp.save();

        res.json({
            ok: true
        });
        
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

//Agregar blackout
app.post('/addblackout', [check('idhotel').exists().withMessage('idhotel required'),
    check('nombre').exists().withMessage('nombre required'),
    check('fechaInicio').exists().withMessage('fechaInicio required'),
    check('fechaFinal').exists().withMessage('fechaFinal required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async(req,res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let temp = new Temporada({
            idhotel: req.body.idhotel,
            nombre: req.body.nombre,
            tipo: 'BOUT',
            fechaInicio: new Date(req.body.fechaInicio),
            fechaFinal: new Date(req.body.fechaFinal)
        });

        let resT = await validaFechas(temp.idhotel, temp.fechaInicio, temp.fechaFinal, 'BOUT');

        if (!resT) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Las fechas se traslapan"}`);
            return res.status(500).json({
                ok: false,
                err: { code: 400, msg: "Operation Failed", details: `Las fechas se traslapan` }
            });
        }

        res.json({
            ok: true
        });

    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

app.get('/alltemporadas/:id*?', [verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req,res) => {

    try {
        
        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let idhotel = req.admin.idhotel;

        if (req.admin.role == 'ROOT') {
            idhotel = req.params.id;
            if (!idhotel) {
                let temporadas = await Temporada.find({ tipo: 'NORMAL' }).exec();

                res.json({
                    ok: true,
                    temporadas
                });
            } else {
                let temporadas = await Temporada.find({ idhotel, tipo: 'NORMAL' }).exec();

                res.json({
                    ok: true,
                    temporadas
                });
            }
        } else {
            if (!idhotel) {
                log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Missing idhotel"}`);
                return res.status(500).json({
                    ok: false,
                    err: { code: 400, msg: "Operation Failed", details: "Missing idhotel" }
                });
            }
            let temporadas = await Temporada.find({ idhotel, tipo: 'NORMAL' }).exec();

            res.json({
                ok: true,
                temporadas
            });
        }
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });        
    }
});

app.get('/allrevenues/:id*?', [verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {
        
        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let idhotel = req.admin.idhotel;

        if (req.admin.role == 'ROOT') {
            idhotel = req.params.id;
            if (!idhotel) {
                let revs = await Temporada.find({ tipo: 'REV' }).exec();

                res.json({
                    ok: true,
                    revs
                });
            } else {
                let revs = await Temporada.find({ idhotel, tipo: 'REV' }).exec();

                res.json({
                    ok: true,
                    revs
                });
            }
        } else {
            if (!idhotel) {
                log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Missing idhotel"}`);
                return res.status(500).json({
                    ok: false,
                    err: { code: 400, msg: "Operation Failed", details: "Missing idhotel" }
                });
            }
            let revs = await Temporada.find({ idhotel, tipo: 'REV' }).exec();

            res.json({
                ok: true,
                revs
            });
        }
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });        
    }
});

app.get('/allblackouts/:id*?', [verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {
        
        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let idhotel = req.admin.idhotel;

        if (req.admin.role == 'ROOT') {
            idhotel = req.params.id;
            if (!idhotel) {
                let blackouts = await Temporada.find({ tipo: 'BOUT' }).exec();

                res.json({
                    ok: true,
                    blackouts
                });
            } else {
                let blackouts = await Temporada.find({ idhotel, tipo: 'BOUT' }).exec();

                res.json({
                    ok: true,
                    blackouts
                });
            }
        } else {
            if (!idhotel) {
                log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Missing idhotel"}`);
                return res.status(500).json({
                    ok: false,
                    err: { code: 400, msg: "Operation Failed", details: "Missing idhotel" }
                });
            }
            let blackouts = await Temporada.find({ idhotel, tipo: 'BOUT' }).exec();

            res.json({
                ok: true,
                blackouts
            });
        }
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });        
    }
});

app.put('/update/:id', [check('id').exists().withMessage('id required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req,res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let id = req.params.id;
        let idhotel = req.body.idhotel;
        let tipo = req.body.tipo;
        let aux = _.pick(req.body, ["nombre", "fechaInicio", "fechaFinal"]);
        let body = {
            nombre: aux.nombre,
            fechaInicio: new Date(aux.fechaInicio),
            fechaFinal: new Date(aux.fechaFinal)
        };

        let resT = await validaFechas(idhotel, body.fechaInicio, body.fechaFinal, tipo, id);

        if (!resT) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Las fechas se traslapan"}`);
            return res.status(500).json({
                ok: false,
                err: { code: 400, msg: "Operation Failed", details: `Las fechas se traslapan` }
            });
        }

        let res1 = await Temporada.findOneAndUpdate({ _id: id }, body, { new: true });

        res.json({
            ok: true
        });
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

app.delete('/delete/:id', [check('id').exists().withMessage('id required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req,res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let id = req.params.id;

        let res1 = await Temporada.findByIdAndRemove({ _id: id });

        res.json({
            ok: true
        });        
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

function guardaLog(idhotel, actividad, idadmin) {
    return new Promise(resolve => {
        let fecha = new Date();
        let log = new Log({
            idhotel,
            actividad,
            idadmin,
            fecha
        });

        log.save((err) => {
            if (err != null) {
                resolve(false);
            }

            resolve(true);
        });
    });
}

function validaFechas(idhotel, fecha1, fecha2, tipo, idAux) {
    let id = idAux || null;
    return new Promise(resolve => {
        Temporada.find({
            idhotel, tipo, $and: [{
                $nor: [{ $and: [{ fechaInicio: { $gt: fecha1 } }, { fechaInicio: { $gt: fecha2 } }] },
                { $and: [{ fechaFinal: { $lt: fecha1 } }, { fechaFinal: { $lt: fecha2 } }] }]
            }, { _id: { $ne: id } }]
        }).exec((err, temporadas) => {

            if (err != null) {
                resolve(false);
            }
            console.log(temporadas);
            if ((temporadas === undefined) || (temporadas.length > 0)) {
                resolve(false);
            }
            else {
                resolve(true);
            }
        })
    });
}

module.exports = app;