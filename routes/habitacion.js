//Paquetes de node
const _ = require('underscore');
const express = require('express');
const { check, validationResult, matchedData } = require('express-validator');
//Importaciones locales
const Habitacion = require('../models/habitacion');
const Temporada = require('../models/temporada');
const Cama =  require('../models/cama');
const Log = require('../models/log');
const log = require('../services/apilogger');
const { verificaToken, verificaTokenAdmin, verificaAdminHotel } = require('../middlewares/auth');

const app = express();

app.post('/add', [check('idhotel').exists().withMessage('idhotel is required'),
    check('nombre').exists().withMessage('nombre is required'),
    check('descripcion').exists().withMessage('descripcion is required'),
    check('ocupacion_max').exists().withMessage('ocupacion_max is required'),
    check('num_total').exists().withMessage('num_total is required'),
    check('precioBase').exists().withMessage('precioBase is required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req,res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let hab;
        let camasAF = [];

        if (req.body.ocupacion.camas && req.body.ocupacion.tarifas && req.body.ocupacion.revenues) {

            if (!(await validateTarifas(req.body.ocupacion.tarifas))) {
                log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Temporada not found in temporadas"}`);
                return res.status(500).json({
                    ok: false,
                    err: { code: 400, msg: "Operation Failed", details: "Temporada not found in tarifas" }
                });
            }
            
            if (!(await validateRevenues(req.body.ocupacion.revenues))) {
                log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Temporada not found in revenues"}`);
                return res.status(500).json({
                    ok: false,
                    err: { code: 400, msg: "Operation Failed", details: "Temporada not found in renenues" }
                });
            } 
            
            if (!(await validateCamas(req.body.ocupacion.camas))) {
                log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Cama not found in camas"}`);
                return res.status(500).json({
                    ok: false,
                    err: { code: 400, msg: "Operation Failed", details: "Cama not found in camas" }
                });
            }

            for (let camaAux of req.body.ocupacion.camas) {

                camaAux.cantidad = camaAux.cantidad ? camaAux.cantidad : 1;
                _id = camaAux.cama;

                let realCama = await Cama.findOne({ _id });

                let avialableReal = realCama.total - (realCama.ocupadas + camaAux.cantidad);
                let avialableFlex = realCama.total - (realCama.asignadas + camaAux.cantidad);

                if ((avialableReal < 0) || (camaAux.cantidad > realCama.total)) {
                    log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Cantidad insuficiente de cama: '${realCama.nombre}' para compleltar la operación"}`);
                    return res.status(500).json({
                        ok: false,
                        err: { code: 400, msg: "Operation Failed", details: `Cantidad insuficiente de cama: '${realCama.nombre}' para compleltar la operación` }
                    });
                }

                if (avialableFlex < 0) {
                    camasAF.push(realCama.nombre);
                }

                let res3 = await Cama.findByIdAndUpdate({ _id }, { asignadas: (realCama.asignadas + camaAux.cantidad) });
            }

            hab = new Habitacion({
                idhotel: req.body.idhotel,
                nombre: req.body.nombre,
                descripcion: req.body.descripcion,
                ocupacion_max: req.body.ocupacion_max,
                max_children: req.body.max_children,
                precioBase: req.body.precioBase,
                num_total: req.body.num_total,
                ocupacion: req.body.ocupacion,
            });
        } else {
            hab = new Habitacion({
                idhotel: req.body.idhotel,
                nombre: req.body.nombre,
                descripcion: req.body.descripcion,
                ocupacion_max: req.body.ocupacion_max,
                max_children: req.body.max_children,
                precioBase: req.body.precioBase,
                num_total: req.body.num_total,
            });
        }

        let res1 = await hab.save();

        let log_bien = await guardaLog(req.body.idhotel, "Habitación agregada", req.admin._id);

        res.status(200).json({
            ok: true,
            warning: camasAF
        });
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

// Habitacion.findOneAndUpdate({_id:id, idhotel}, {$push: {camas_disponibles: cama_nueva}}, async(err,habitacion) => {

app.get('/all/:id*?', [verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req,res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let idhotel = req.admin.idhotel;

        if (req.admin.role == 'ROOT') {
            idhotel = req.params.id;
            if (!idhotel) {
                let habitaciones = await Habitacion.find({})
                    .populate('ocupacion.camas.cama', 'nombre')
                    .populate('ocupacion.revenues.revenue', 'nombre')
                    .populate('ocupacion.tarifas.temporada', 'nombre').exec();

                res.json({
                    ok: true,
                    habitaciones
                });
            } else {
                let habitaciones = await Habitacion.find({ idhotel })
                    .populate('ocupacion.camas.cama', 'nombre')
                    .populate('ocupacion.revenues.revenue', 'nombre')
                    .populate('ocupacion.tarifas.temporada', 'nombre').exec();

                res.json({
                    ok: true,
                    habitaciones
                });
            }
        } else {
            if (!idhotel) {
                log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Missing idhotel"}`);
                return res.status(500).json({
                    ok: false,
                    err: { code: 400, msg: "Operation Failed", details: "Missing idhotel" }
                });
            }
            let habitaciones = await Habitacion.find({ idhotel })
                .populate('ocupacion.camas.cama', 'nombre')
                .populate('ocupacion.revenues.revenue', 'nombre')
                .populate('ocupacion.tarifas.temporada', 'nombre').exec();

            res.json({
                ok: true,
                habitaciones
            });
        }

    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

app.delete('/delete/:id', [check('id').exists().withMessage('id required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req,res) => {
    
    try {
        
        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let id = req.params.id;

        let habX = await Habitacion.findOneAndDelete({ _id: id });

        for (let camaDeleted of habX.ocupacion.camas) {

            let camaFinded = await Cama.findOne({ _id: camaDeleted.cama });

            let cantidadNew = camaFinded.asignadas - camaDeleted.cantidad;
            
            if (cantidadNew >= 0 ) {
                let resXC = await Cama.findOneAndUpdate({ _id: camaFinded._id }, { asignadas: cantidadNew });
            }
        } 

        res.json({
            ok: true
        });
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

app.put('/update/:id', [check('id').exists().withMessage('id required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {
    
    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let id = req.params.id;
        let idhotel = req.body.idhotel;
        let body = _.pick(req.body, ["nombre", "ocupacion_max", "descripcion", "max_children", "precioBase", "num_total"]);

        let res1 = await Habitacion.findOneAndUpdate({ _id: id, idhotel }, body, { new: true, runValidators: true });

        let log_bien = await guardaLog(idhotel, "Habitación editada", req.admin._id);
        
        res.json({
            ok: true
        });
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

//Agrega una temporada a la ocupacion
app.post('/addtarifa/:id', [check('id').exists().withMessage('id required'), 
    check('temporada').exists().withMessage('temporada required'),
    check('precio').exists().withMessage('precio required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let id = req.params.id;
        let tarifa = {
            temporada: req.body.temporada,
            precio: req.body.precio
        }

        if (!(await validateTarifa(tarifa))) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Temporada not found"}`);
            return res.status(500).json({
                ok: false,
                err: { code: 400, msg: "Operation Failed", details: "Temporada not found" }
            });
        }

        let res1 = await Habitacion.findOneAndUpdate({ _id: id }, { $push: { "ocupacion.tarifas": tarifa } });

        res.json({
            ok: true
        });        
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

//Agrega un revenue a la ocupacion
app.post('/addrenevue/:id', [check('id').exists().withMessage('id required'),
    check('revenue').exists().withMessage('revenue required'),
    check('precio').exists().withMessage('precio required'),  verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let id = req.params.id;
        let revenue = {
            revenue: req.body.revenue,
            precio: req.body.precio
        }

        if (!(await validateRevenue(revenue))) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Temporada/Revenue not found"}`);
            return res.status(500).json({
                ok: false,
                err: { code: 400, msg: "Operation Failed", details: "Temporada/Revenue not found" }
            });
        }

        let res1 = await Habitacion.findOneAndUpdate({ _id: id }, { $push: { "ocupacion.revenues": revenue } });

        res.json({
            ok: true
        });
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

app.delete('/resetocupacion/:id', [
    check('id').exists().withMessage('id required'), verificaToken, 
    verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let id = req.params.id;

        let habX = await Habitacion.findOne({ _id: id });

        for (let camaDeleted of habX.ocupacion.camas) {

            let camaFinded = await Cama.findOne({ _id: camaDeleted.cama });

            let cantidadNew = camaFinded.asignadas - camaDeleted.cantidad;

            if (cantidadNew >= 0) {
                let resXC = await Cama.findOneAndUpdate({ _id: camaFinded._id }, { asignadas: cantidadNew });
            }
        } 

        let res1 = await Habitacion.findOneAndUpdate({ _id: id }, { ocupacion: null });

        res.json({
            ok: true
        });
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

// Agrega una cama a la habitación
app.post('/addcama/:id', [check('id').exists().withMessage('id required'), 
    check('cama').exists().withMessage('cama required'), 
    check('cantidad').exists().withMessage('cantidad required'),verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let idHab = req.params.id;
        let warning = 0;
        let camaNew = {
            cama: req.body.cama,
            cantidad: req.body.cantidad
        }

        if (!(await validateCama(camaNew))) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Cama not found"}`);
            return res.status(500).json({
                ok: false,
                err: { code: 400, msg: "Operation Failed", details: "Cama not found" }
            });
        }

        let realCama = await Cama.findOne({ _id: camaNew.cama });

        let avialableReal = realCama.total - (realCama.ocupadas + camaNew.cantidad);
        let avialableFlex = realCama.total - (realCama.asignadas + camaNew.cantidad);

        if ((avialableReal < 0) || (camaNew.cantidad > realCama.total)) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Cantidad insuficiente de cama: '${realCama.nombre}' para compleltar la operación"}`);
            return res.status(500).json({
                ok: false,
                err: { code: 400, msg: "Operation Failed", details: `Cantidad insuficiente de cama: '${realCama.nombre}' para compleltar la operación` }
            });
        }

        if (avialableFlex < 0) {
            warning = 1;
        }

        let res2 = await Cama.findByIdAndUpdate({ _id }, { asignadas: (realCama.asignadas + camaNew.cantidad) });

        let res3 = await Habitacion.findOneAndUpdate({ _id: idHab }, { $push: { "ocupacion.camas": camaNew } });

        res.json({
            ok: true,
            warning
        });        
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

app.put('/updatetarifa/:id', [check('id').exists().withMessage('id required'),
    check('id_tarifa').exists().withMessage('id_tarifa required'),
    check('precio').exists().withMessage('precio required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {
    
    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let id = req.params.id;
        let id_tarifa = req.body.id_tarifa;
        let precio = req.body.precio;

        let res1 = await Habitacion.updateOne({ _id: id, 'ocupacion.tarifas._id' : id_tarifa },{ $set: { "ocupacion.tarifas.$.precio": precio } });

        res.json({
            ok: true
        });
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

app.put('/updaterevenue/:id', [check('id').exists().withMessage('id required'),
    check('id_revenue').exists().withMessage('id_revenue required'),
    check('precio').exists().withMessage('precio required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let id = req.params.id;
        let id_revenue = req.body.id_revenue;
        let precio = req.body.precio;

        let res1 = await Habitacion.updateOne({ _id: id, 'ocupacion.revenues._id': id_revenue }, { $set: { "ocupacion.revenues.$.precio": precio } });

        res.json({
            ok: true
        });
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

app.put('/updatecama/:id', [check('id').exists().withMessage('id required'),
    check('id_cama').exists().withMessage('id_cama required'),
    check('cantidad').exists().withMessage('cantidad required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let id = req.params.id;
        let id_cama = req.body.id_cama;
        let cantidadN = req.body.cantidad; 
        let warning = 0;

        let hab = await Habitacion.findOne({ _id: id });

        let camaUpdate = hab.ocupacion.camas.find(element => element._id == id_cama);

        let camaFinded = await Cama.findOne({ _id: camaUpdate.cama });

        let avialableReal = camaFinded.total - (camaFinded.ocupadas + cantidadN);
        let avialableFlex = camaFinded.total - ((camaFinded.asignadas - camaUpdate.cantidad) + cantidadN);

        if ((avialableReal < 0) || (cantidadN > camaFinded.total)) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Cantidad insuficiente de cama: '${camaFinded.nombre}' para compleltar la operación"}`);
            return res.status(500).json({
                ok: false,
                err: { code: 400, msg: "Operation Failed", details: `Cantidad insuficiente de cama: '${camaFinded.nombre}' para compleltar la operación` }
            });
        }

        if (avialableFlex < 0) {
            warning = 1;
        }

        let res1 = await Habitacion.updateOne({ _id: id, 'ocupacion.camas._id': id_cama }, { $set: { "ocupacion.camas.$.cantidad": cantidadN } });
        let res2 = await Cama.findByIdAndUpdate({ _id: camaFinded._id }, { asignadas: ((camaFinded.asignadas - camaUpdate.cantidad) + cantidadN) });

        res.json({
            ok: true,
            warning
        });

    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

app.delete('/deletecama/:id', [check('id').exists().withMessage('id required'), 
    check('id_cama').exists().withMessage('id_cama required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let id = req.params.id;
        let id_cama = req.body.id_cama;

        let hab = await Habitacion.findOne({ _id: id });

        let camaDeleted = hab.ocupacion.camas.find(element => element._id == id_cama);

        let camaFinded = await Cama.findOne({ _id: camaDeleted.cama });

        let cantidadNew = camaFinded.asignadas - camaDeleted.cantidad;

        if (cantidadNew >= 0) {
            let resXC = await Cama.findOneAndUpdate({ _id: camaFinded._id }, { asignadas: cantidadNew });
        }

        hab.ocupacion.camas.remove(id_cama);

        let res1 = await hab.save();

        res.json({
            ok: true
        });
        
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

app.delete('/deletetarifa/:id', [check('id').exists().withMessage('id required'),
check('id_tarifa').exists().withMessage('id_tarifa required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let id = req.params.id;
        let id_tarifa = req.body.id_tarifa;

        let hab = await Habitacion.findOne({ _id: id });

        hab.ocupacion.tarifas.remove(id_tarifa);

        let res1 = await hab.save();

        res.json({
            ok: true
        });

    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

app.delete('/deleterevenue/:id', [check('id').exists().withMessage('id required'),
check('id_revenue').exists().withMessage('id_revenue required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let id = req.params.id;
        let id_revenue = req.body.id_revenue;

        let hab = await Habitacion.findOne({ _id: id });

        hab.ocupacion.revenues.remove(id_revenue);

        let res1 = await hab.save();

        res.json({
            ok: true
        });

    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

function guardaLog(idhotel, actividad, idadmin) {
    return new Promise(resolve => {
        let fecha = new Date();
        let log = new Log({
            idhotel,
            actividad,
            idadmin,
            fecha
        });

        log.save((err) => {
            if (err != null) {
                resolve(false);
            }

            resolve(true);
        });
    });
}

async function validateTarifas(temps) {

    for (let temp of temps) {
        let resX = await Temporada.countDocuments({ _id: temp.temporada, tipo: 'NORMAL' });
        if (resX <= 0) {
            return false;
        }
    }

    return true;
}

async function validateTarifa(temp) {
    
    let resX = await Temporada.countDocuments({ _id: temp.temporada, tipo: 'NORMAL' });
    if (resX <= 0) {
        return false;
    }

    return true;
}

async function validateRevenue(rev) {

    let resX = await Temporada.countDocuments({ _id: rev.revenue, tipo: 'REV' });
    if (resX <= 0) {
        return false;
    }

    return true;
}

async function validateRevenues(revs) {

    for (let rev of revs) {
        let resX = await Temporada.countDocuments({ _id: rev.revenue, tipo: 'REV' });
        if (resX <= 0) {
            return false;
        }
    }

    return true;
}

async function validateCama(camaX) {

    let resX = await Cama.countDocuments({ _id: camaX.cama });
    if (resX <= 0) {
        return false;
    }

    return true
}

async function validateCamas(camas) {

    for (let camaX of camas) {
        let resX = await Cama.countDocuments({ _id: camaX.cama });
        if (resX <= 0) {
            return false;
        }
    }

    return true
}


module.exports = app;