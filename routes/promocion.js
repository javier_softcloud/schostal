//Paquetes de node
const express = require('express');
const _ = require('underscore');
const { check, validationResult, matchedData } = require('express-validator');
//Importaciones locales
const Promocion = require('../models/promocion');
const Log = require('../models/log');
const log = require('../services/apilogger');
const { verificaToken, verificaTokenAdmin, verificaAdminHotel } = require('../middlewares/auth');

const app = express();

//Agregar una promocion
app.post('/add', [check('idhotel').exists().withMessage('idhotel required'),
    check('tipo').exists().withMessage('tipo required'),
    check('terminos').exists().withMessage('terminos required'),
    check('porcentajeDesc').exists().withMessage('porcentajeDesc required'),
    check('fechaInicio').exists().withMessage('fechaInicio required'),
    check('fechaFinal').exists().withMessage('fechaFinal required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let temp = new Promocion({
            idhotel: req.body.idhotel,
            tipo: req.body.tipo,
            terminos: req.body.terminos,
            porcentajeDesc: req.body.porcentajeDesc,
            fechaInicio: req.body.fechaInicio,
            fechaFinal: req.body.fechaFinal
        });

        let res1 = await temp.save();

        let log_bien = await guardaLog(req.body.idhotel, "Promoción agregada", req.admin._id);
        
        res.json({
            ok: true
        });
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        }); 
    }
});

app.get('/all/:id*?', [verificaToken, verificaTokenAdmin], async (req, res) => {
    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let idhotel = req.admin.idhotel;

        if (req.admin.role == 'ROOT') {
            idhotel = req.params.id;
            if (!idhotel) {
                let promociones = await Promocion.find({}).exec();

                res.json({
                    ok: true,
                    promociones
                });
            } else {
                let promociones = await Promocion.find({ idhotel }).exec();

                res.json({
                    ok: true,
                    promociones
                });
            }
        } else {
            if (!idhotel) {
                log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"Missing idhotel"}`);
                return res.status(500).json({
                    ok: false,
                    err: { code: 400, msg: "Operation Failed", details: "Missing idhotel" }
                });
            }
            let promociones = await Promocion.find({ idhotel }).exec();

            res.json({
                ok: true,
                promociones
            });
        }
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

app.put('/update/:id', [check('id').exists().withMessage('id required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }
        
        let id = req.params.id;
        let idhotel = req.body.idhotel;
        let body = _.pick(req.body, ["tipo", "terminos", "porcentajeDesc", "fechaInicio", "fechaFinal"]);

        let res1 = await Promocion.findOneAndUpdate({ _id: id, idhotel }, body, { new: true });

        res.json({
            ok: true
        });
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

app.delete('/delete/:id', [check('id').exists().withMessage('id required'), verificaToken, verificaTokenAdmin, verificaAdminHotel], async (req, res) => {

    try {

        log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "admin":"${req.admin._id}"}`);

        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.admin._id}","error":"${errors.mapped()}"}`);
            return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
        }

        let id = req.params.id;

        let res1 = await Promocion.findByIdAndRemove({ _id: id });

        let log_bien = await guardaLog(idhotel, "Promoción agregada", req.admin._id);

        res.json({
            ok: true
        });
    } catch (err) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","admin":"${req.admin._id}", "error":"${err}"}`);
        return res.status(500).json({
            ok: false,
            err: { code: 400, msg: "Operation Failed", details: err }
        });
    }
});

function guardaLog(idhotel, actividad, idadmin) {
    return new Promise(resolve => {
        let fecha = new Date();
        let log = new Log({
            idhotel,
            actividad,
            idadmin,
            fecha
        });

        log.save((err) => {
            if (err != null) {
                resolve(false);
            }

            resolve(true);
        });
    });
}

module.exports = app;