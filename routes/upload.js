const express = require('express');
const fileUpload = require('express-fileupload');
const Usuario = require('../models/usuario');
const Admin = require('../models/admin');
const Hotel = require('../models/hotel');
const Habitacion = require('../models/habitacion');
const fs = require('fs');
const path = require('path');
const {verificaToken} = require('../middlewares/auth');

const app = express();

app.use(fileUpload());

app.post('/slider/:tipo/:id', [verificaToken], (req,res) => {

    let tipo = req.params.tipo;
    let id = req.params.id;

    if(Object.keys(req.files).length == 0){
        return res.status(400).json({
            ok: false,
            err: "No hay archivos"
        });
    }

    let validtypes = ['hotel','habitacion'];

    if(validtypes.indexOf(tipo) < 0){
        return res.status(400).json({
            ok: false,
            err: "Ruta invalida"
        });
    }

    let archivos = req.files.galeria;
    let validextentions = ['png', 'jpg', 'gif', 'jpeg'];
    let i = 0;
    let archivos_aux = [];

    for(let archivo of archivos){

        let partes = archivo.name.split('.');
        let extention = partes[partes.length - 1];

        if (validextentions.indexOf(extention) < 0) {
            return res.status(400).json({
                ok: false,
                err: "Archivo no permitido"
            });
        }

        let time = Math.trunc((new Date().getTime()) / 1000);

        let realName = `${id}-${time}-${i}.${extention}`;
        i++;

        archivo.mv(`uploads/${tipo}/${realName}`, (err) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }

            archivos_aux.push(realName);
        });
    }

    switch (tipo) {
        case 'hotel':
            imgHotel(id, res, archivos_aux);
            break;
        case 'habitacion':
            imgHab(id, res, archivos_aux);
            break;
    }
});


app.post('/:tipo/:id', [verificaToken], (req,res) => {

    let tipo = req.params.tipo;
    let id = req.params.id;

    if(Object.keys(req.files).length == 0){
        return res.status(400).json({
            ok: false,
            err: "No hay archivo"
        });
    }

    let validtypes = ['usuario','admin','recepcionista'];

    if(validtypes.indexOf(tipo) < 0){
        return res.status(400).json({
            ok: false,
            err: "Ruta invalida"
        });
    }

    let archivo = req.files.img;

    let partes = archivo.name.split('.');
    let extention = partes[partes.length-1];

    let validextentions = ['png','jpg','gif','jpeg'];

    if(validextentions.indexOf(extention) < 0){
        return res.status(400).json({
            ok: false,
            err: "Archivo no permitido"
        });
    }

    let time = Math.trunc((new Date().getTime()) / 1000);

    let realName = `${id}-${time}.${extention}`;

    archivo.mv(`uploads/${tipo}/${realName}`, (err) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            });
        }

        switch(tipo){
            case 'usuario':
                imgUsuario(id,res,realName);
            break;
            case 'admin':
                imgAdmin(id,res,realName);
            break;
            case 'recepcionista':
                imgRecepcionista(id,res,realName);
            break;
        }
    });


});

function imgUsuario(id,res,name){
    Usuario.findOne({_id:id}, (err,usuario) => {
        if(err){
            borraRepetida('usuario',name);
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(!usuario){
            borraRepetida('usuario',name);
            return res.status(500).json({
                ok: false,
                err: "No existe el cliente"
            });
        }

        borraRepetida('usuario',usuario.img);

        usuario.img = name;

        usuario.save((err, usuarioR) => {
            res.json({
                ok:true,
                usuario: usuarioR
            });
        });
    });
}

function imgAdmin(id,res,name){
    Admin.findOne({_id:id, role:"ADMIN_ROLE"}, (err,admin) => {
        if(err){
            borraRepetida('admin',name);
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(!admin){
            borraRepetida('admin',name);
            return res.status(500).json({
                ok: false,
                err: "No existe el admin"
            });
        }

        borraRepetida('admin',admin.img);

        admin.img = name;

        admin.save((err, adminR) => {
            res.json({
                ok:true,
                admin: adminR
            });
        });
    });
}

function imgRecepcionista(id,res,name){
    Admin.findOne({_id:id, role:"RECEP_ROLE"}, (err,recepcionista) => {
        if(err){
            borraRepetida('recepcionista',name);
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(!recepcionista){
            borraRepetida('recepcionista',name);
            return res.status(500).json({
                ok: false,
                err: "No existe el recepcionista"
            });
        }

        borraRepetida('recepcionista',recepcionista.img);

        recepcionista.img = name;

        recepcionista.save((err, recepcionistaR) => {
            res.json({
                ok:true,
                recepcionista: recepcionistaR
            });
        });
    });
}
//Habitacion.updateOne({ _id: id }, { $set: { camas_disponibles: camas } }
function imgHotel(id, res, archivos_aux){

    Hotel.findOne({_id:id}, (err, hotel) => {
        if(err != null){
            borraRepetidaMultiple('hotel', archivos_aux);
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(!hotel){
            borraRepetidaMultiple('hotel', archivos_aux);
            return res.status(500).json({
                ok: false,
                err
            });
        }

        borraRepetidaMultiple('hotel',hotel.galeria);

        hotel.galeria = archivos_aux;

        hotel.save( (err,hotelR) => {
            res.json({
                ok:true,
                hotel: hotelR
            });
        });

    });
}

function imgHab(id, res, archivos_aux){

    Habitacion.findOne({_id:id}, (err, habitacion) => {
        if(err != null){
            borraRepetidaMultiple('habitacion', archivos_aux);
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(!habitacion){
            borraRepetidaMultiple('habitacion', archivos_aux);
            return res.status(500).json({
                ok: false,
                err
            });
        }

        borraRepetidaMultiple('habitacion',habitacion.galeria);

        habitacion.galeria = archivos_aux;

        habitacion.save( (err,habitacionR) => {
            res.json({
                ok:true,
                habitacion: habitacionR
            });
        });

    });
}

function borraRepetida(tipo,img){
    let pathImg = path.resolve(__dirname, `../uploads/${tipo}/${img}`);

    if (fs.existsSync(pathImg)) {
        fs.unlinkSync(pathImg);
    }
}

function borraRepetidaMultiple(tipo, imgs) {

    for(let img of imgs){
        let pathImg = path.resolve(__dirname, `../uploads/${tipo}/${img}`);

        if (fs.existsSync(pathImg)) {
            fs.unlinkSync(pathImg);
        }
    }
}

module.exports = app;
