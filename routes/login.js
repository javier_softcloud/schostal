const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { check, validationResult, matchedData } = require('express-validator');

const {OAuth2Client} = require('google-auth-library');
const client =  new OAuth2Client(process.env.CLIENT_ID);

const speakeasy = require('speakeasy');

const Usuario = require('../models/usuario');
const log = require('../services/apilogger');
const app = express();

//Login usuario
app.post('/login', [check('email').exists().withMessage('Email required')
  .isEmail().withMessage('Email not valid'),
  check('password').exists().withMessage('Password required')], async (req,res) => {

    try {

      log.logger.info(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "email":"${req.body.email}"}`);

      let errors = validationResult(req);
      if (!errors.isEmpty()) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"${req.body.email}","error":"${errors.mapped()}"}`);
        return res.status(400).json({ ok: false, error: { code: 201, msg: "Request has invalid data", details: errors.mapped() } });
      }

      let email = req.body.email;
      let pass = req.body.password;


        let usuarioDB = await Usuario.findOne({ email });

      if (!usuarioDB) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"Not Logged", "error":"User not found"}`);
        return res.status(500).json({
          ok: false,
          err: { code: 400, msg: "Operation Failed", details: "User not found" }
        });
      }

      if (!bcrypt.compareSync(pass, usuarioDB.password)) {
        log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"Not Logged", "error":"Invalid password"}`);
        return res.status(500).json({
          ok: false,
          err: { code: 400, msg: "Operation Failed", details: "Invalid password" }
        });
      }

      let token = jwt.sing({
        usuario: usuarioDB
      }, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN });

      res.json({
        ok: true,
        usuario: usuarioDB,
        token
      });

    } catch (err) {
      log.logger.error(`{"verb":"${req.method}", "path":"${req.baseUrl + req.path}", "params":"${JSON.stringify(req.params)}", "query":"${JSON.stringify(req.query)}", "body":"${JSON.stringify(req.body)}","email":"Not Logged", "error":"${err}"}`);
      return res.status(500).json({
        ok: false,
        err: { code: 400, msg: "Operation Failed", details: err }
      });
    }
});

//Login root
app.post('/auth', (req,res) => {
  let utoken = req.body.token;

  let verified = speakeasy.totp.verify({
  secret: process.env.CADUCIDAD_TOKEN,
  encoding: 'base32',
  token: utoken
  });

  res.json({verified})
});

/*
admin{
  nombre,
  email,
  password,
  idhotel
}

habitaciones{
  nombre,
  camas:[],
  tipo,
  activos:[],

}
*/
