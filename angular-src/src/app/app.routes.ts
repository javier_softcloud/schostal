
import { RouterModule, Routes } from '@angular/router';
import {PageComponent} from "./components/page/page.component";
import {AdminComponent} from "./components/admin/admin.component";
import {InicioComponent} from "./components/page/inicio/inicio.component";
import {NosotrosComponent} from "./components/page/nosotros/nosotros.component";
import {ReservacionComponent} from "./components/page/reservacion/reservacion.component";
import {ServiciosComponent} from "./components/page/servicios/servicios.component";
import {ContactoComponent} from "./components/page/contacto/contacto.component";
import {LoginAdminComponent} from "./components/admin/login-admin/login-admin.component";
import {DashboardAdminComponent} from "./components/admin/dashboard-admin/dashboard-admin.component";
import {AuthAdminGuard} from "./guards/auth-admin.guard";
import {AdministradoresComponent} from "./components/admin/dashboard-admin/administradores/administradores.component";
import {ClientesComponent} from "./components/admin/dashboard-admin/clientes/clientes.component";
import {MensajesContactoComponent} from "./components/admin/dashboard-admin/mensajes-contacto/mensajes-contacto.component";
import {HotelesComponent} from "./components/admin/dashboard-admin/hoteles/hoteles.component";
import {HabitacionesComponent} from "./components/admin/dashboard-admin/habitaciones/habitaciones.component";
import {ActivosCamasComponent} from "./components/admin/dashboard-admin/activos-camas/activos-camas.component";
import {UserProfileComponent} from "./components/page/user-profile/user-profile.component";
import {EditarPerfilComponent} from "./components/page/user-profile/editar-perfil/editar-perfil.component";
import {ConsumiblesComponent} from "./components/admin/dashboard-admin/consumibles/consumibles.component";
import {TemporadasComponent} from "./components/admin/dashboard-admin/temporadas/temporadas.component";
import {ReservacionUsuarioComponent} from "./components/page/reservacion-usuario/reservacion-usuario.component";
import {ReservacionCompletadaComponent} from "./components/page/inicio/reservacion-completada/reservacion-completada.component";
import {ReservacionesUsuarioComponent} from "./components/page/user-profile/reservaciones-usuario/reservaciones-usuario.component";
import {ReservacionesComponent} from "./components/admin/dashboard-admin/reservaciones/reservaciones.component";
import {ClienteComponent} from "./components/admin/dashboard-admin/clientes/cliente/cliente.component";


const APP_ROUTES: Routes = [
  { path: 'home', component: PageComponent,
      children:[
        { path: 'inicio', component: InicioComponent },
        { path: 'nosotros', component: NosotrosComponent },
        { path: 'reservacion', component: ReservacionComponent },
        { path: 'perfil', component: UserProfileComponent, children:[
            { path: 'editar', component: EditarPerfilComponent },
            { path: 'notificaciones', component: EditarPerfilComponent },
            { path: 'reservaciones', component: ReservacionesUsuarioComponent },
          ] },
        { path: 'servicios', component: ServiciosComponent },
        { path: 'contacto', component: ContactoComponent },
        { path: 'reservacion-completada', component: ReservacionCompletadaComponent },
        { path: 'reservar', component: ReservacionUsuarioComponent }

      ]},
  { path: 'admin', component: AdminComponent,
    children:[
      { path: 'login', component:LoginAdminComponent  },
      { path: 'dashboard', component: DashboardAdminComponent,canActivate:[AuthAdminGuard], children:[
          { path: 'administradores', component: AdministradoresComponent },
          { path: 'usuarios', component: ClientesComponent},
          { path: 'clientes/:id', component: ClienteComponent},
          { path: 'mensajescontacto', component: MensajesContactoComponent },
          { path: 'hoteles', component: HotelesComponent },
          { path: 'habitaciones', component: HabitacionesComponent },
          { path: 'activoscamas', component: ActivosCamasComponent },
          { path: 'temporadas', component: TemporadasComponent },
          { path: 'consumibles', component: ConsumiblesComponent },
          { path: 'reservaciones', component: ReservacionesComponent},

          { path: '**', pathMatch: 'full', redirectTo: 'administradores'}
        ]},
      { path: '**', pathMatch: 'full', redirectTo: 'login'}
    ] },
  { path: '**', pathMatch: 'full', redirectTo: 'home/inicio' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
