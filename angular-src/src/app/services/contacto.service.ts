import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";


@Injectable({
  providedIn: 'root'
})
export class ContactoService {

  servidor:string=environment.api_host;

  constructor(private  http:HttpClient) {

  }

  enviarMensaje(mensaje:any){
    let body=mensaje;
    let headers:HttpHeaders=new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json'
    });
    let url=this.servidor+"/contactar";
    return this.http.post(url,body,{headers})
  }
}
