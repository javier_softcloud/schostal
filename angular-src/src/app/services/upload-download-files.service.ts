import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UploadDownloadFilesService {
  token_user:string;
  token_admin:string;
  servidor:string;


  constructor( private http:HttpClient) {
    this.token_admin=localStorage.getItem('token_admin')
    this.token_user=localStorage.getItem('token_user')
    this.servidor=environment.api_host;
    console.log(this.servidor)
  }

  uploadUserImg(form_data, user){
    let url=this.servidor+"/upload/cliente/"+user._id;
    let body=form_data;
    let headers =new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept':'application/json',
      'Authorization':this.token_user
    });
    return this.http.put(url,body,{headers})
  }

}
