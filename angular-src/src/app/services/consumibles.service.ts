import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ConsumiblesService {

  servidor:string;
  token:string;

  constructor(private http:HttpClient) {
    this.servidor = environment.api_host;
    this.token = localStorage.getItem('token_admin');
  }

  public addConsumible(activo:any){
    let url=this.servidor+"/consumible/add";
    let body=activo;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });
    return this.http.post(url,body,{headers})
  }

  public editConsumible(activo:any){
    let url=this.servidor+"/consumible/update/"+activo._id;
    let body=activo;
    delete body._id;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });

    return this.http.put(url,body,{headers})
  }

  public getConsumible(){
    let url=this.servidor+"/consumible/all";
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });
    return this.http.get(url,{headers})
  }

  public deleteConsumible(id_activo:string, id_hotel:string){
    let url=this.servidor+"/consumible/delete/"+id_hotel+"/"+id_activo;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });
    return this.http.delete(url,{headers})
  }

}
