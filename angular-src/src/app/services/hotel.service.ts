import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  servidor:string=environment.api_host;
  token:string=localStorage.getItem('token_admin');

  constructor(private http:HttpClient) {

  }

  getHoteles(){
    let url=this.servidor+"/hotel/all";
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });
    return this.http.get(url,{headers})
  }

  getPublicHoteles(){
    let url=this.servidor+"/hotel/public";
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json'
    });
    return this.http.get(url,{headers})
  }

  addHotel(hotel){
    let url=this.servidor+"/hotel/add";
    let body=hotel;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });
    return this.http.post(url,body,{headers})
  }

  deleteHotel(id){
    let url=this.servidor+"/hotel/delete/"+id;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });

    return this.http.delete(url,{headers})
  }

  public editHotel(hotel:any){
    let url = this.servidor + "/hotel/update/" +hotel._id;
    let body=hotel;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });

    return this.http.put(url,body,{headers})
  }
}
