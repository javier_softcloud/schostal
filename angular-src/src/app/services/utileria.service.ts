import { Injectable } from '@angular/core';
import {HotelService} from "./hotel.service";

@Injectable({
  providedIn: 'root'
})
export class UtileriaService {

  constructor() { }


  addCheckItem(items:any[]){
    let items_transformed:any[]=items;
    for(let item in items)
      items[item]["check"]=false;

    return items_transformed;
  }

  checkAllItems(items:any[],verified:boolean){
    let items_checked:any[] = items;
    for(let item in items_checked)
      items_checked[item].check = verified ?  true:false;

    return items_checked;
  }


  sortBy(by: string | any,array:any[],sorted:boolean): any {

    array.sort((a: any, b: any) => {
      if (a[by] < b[by]) {
        return sorted ? 1 : -1;
      }
      if (a[by] > b[by]) {
        return sorted ? -1 : 1;
      }

      return 0;
    });

    return {sorted:!sorted,
            usuarios:array};
  }

  getCheked(items:any[]){
    let array_checked:any[]=[];
    for(let item of items){
      if(item.check){
        array_checked.push(item);
      }
    }

    return array_checked
  }

  public isElement(array:any[]){
    return array.length==1 ? true:false;
  }

  covertToSelect(array:any[]){
    let select_array:any[]=[];
    for(let item of array)
      select_array.push({value:item._id,label:item.nombre})

    return select_array;
  }

  getPropertyById(property:string,array:any[],id:string){
    for(let item of array){
      if(item._id==id)
        return item[property];
    }
    return 'id not found'

  }

  getDayName(date:Date){
    let days = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
    let d = new Date(date);
    let day_name = days[d.getDay()];

    return day_name;
  }

  getMonthName(date){
    let months = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'octubre', 'noviembre', 'diciembre'];
    let m = new Date(date);
    let month_name = months[m.getDay()];

    return month_name;
  }


  getFormatDate(date:string){
    let format_date=new Date(date);

    if(format_date.getMonth()<9)
      if(format_date.getDate()<10)
        return "0"+(format_date.getMonth()+1)+'/0'+ format_date.getDate()+'/'+format_date.getFullYear();
      else
        return "0"+(format_date.getMonth()+1)+'/'+ format_date.getDate()+'/'+format_date.getFullYear();
    else
      if(format_date.getDate()<10)
        return (format_date.getMonth()+1)+'/0'+ format_date.getDate()+'/'+format_date.getFullYear();
      else
        return (format_date.getMonth()+1)+'/'+ format_date.getDate()+'/'+format_date.getFullYear();
  }

  getCompleteDate(date:Date){
    let new_date=new Date(date);
    return this.getDayName(new_date) + ' ' + new_date.getDate() + ' de ' + this.getMonthName(new_date) + ' del ' + new_date.getFullYear()
  }

}
