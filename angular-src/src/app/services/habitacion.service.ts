import { Injectable } from '@angular/core';
import {HttpClient, HttpClientModule, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HabitacionService {

  private servidor=environment.api_host;
  private token:string;

  constructor(public http:HttpClient) {
    this.token=localStorage.getItem('token_admin')

  }

  public addHabitacion(habitacion:any){
    let url=this.servidor+"/habitacion/add";
    let body=habitacion;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });
    return this.http.post(url,body,{headers})
  }

  public getHabitaciones(){
    let url=this.servidor+"/habitacion/all";
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });
    return this.http.get(url,{headers})
  }

  public deleteHabitacion(id:string){
    let url=this.servidor+"/habitacion/delete/"+id;
    console.log(url)
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });

    return this.http.delete(url,{headers})
  }

  public editHabitaciones(habitacion:any){
    let url=this.servidor+"/habitacion/update/"+habitacion._id;
    let body=habitacion;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });

    return this.http.put(url,body,{headers})
  }

  public addTarifa(habitacion_id:string,tarifa:any){
    let url=this.servidor+"/habitacion/addtarifa/" + habitacion_id;
    let body=tarifa;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });
    return this.http.post(url,body,{headers})
  }



}

