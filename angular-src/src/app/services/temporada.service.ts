import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class TemporadaService {

  servidor:string;
  token_user:string;
  token_admin:string;

  constructor(private http: HttpClient) {
    this.servidor=environment.api_host;
    this.token_admin=localStorage.getItem('token_admin');
    this.token_user=localStorage.getItem('token_user');
  }

  addTemporada(temporada:any){
    let body=temporada;
    let url=this.servidor+"/temporada/addtemporada";
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token_admin
    });
    return this.http.post(url,body,{headers})
  }

  getTemporada(id_hotel?:string){
    if(!id_hotel)
      id_hotel="";

    let url=this.servidor+"/temporada/alltemporadas/"+id_hotel;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token_admin
    });
    return this.http.get(url,{headers})
  }

  addRevenue(revenue:any){
    let body=revenue;
    let url=this.servidor+"/temporada/addRevenue";
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token_admin
    });
    return this.http.post(url,body,{headers})
  }

  getRevenue(id_hotel?:string){
    if(!id_hotel)
      id_hotel="";

    let url=this.servidor+"/temporada/allrevenues/"+id_hotel;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token_admin
    });
    return this.http.get(url,{headers})
  }

  addBlackout(blackout:any){
    let body=blackout;
    let url=this.servidor+"/temporada/addBlackout";
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token_admin
    });1
    return this.http.post(url,body,{headers})
  }

  getBlackout(id_hotel?:string){
    if(!id_hotel)
      id_hotel="";

    let url=this.servidor+"/temporada/allblackouts/"+id_hotel;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token_admin
    });
    return this.http.get(url,{headers})
  }

  updateTemporada(temporada:any){
    let body=temporada
    let url=this.servidor+"/temporada/update/"+temporada._id;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token_admin
    });
    return this.http.put(url,body,{headers})
  }

  deleteTemporada(temporada_id:string){
    let url=this.servidor+"/temporada/delete/"+temporada_id;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token_admin
    });
    return this.http.delete(url,{headers})
  }
}
