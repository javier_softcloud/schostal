import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ActivoCamaService {

  servidor:string=environment.api_host;
  token=localStorage.getItem('token_admin');

  constructor(public http:HttpClient) { }

  public addActivo(activo:any){
    let url=this.servidor+"/activo/add";
    let body=activo;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });

    return this.http.post(url,body,{headers})
  }

  public addCama(cama:any){
    let url=this.servidor+"/cama/add";
    let body=cama;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });

    return this.http.post(url,body,{headers})
  }

  public editActivo(activo:any){
    let url=this.servidor+"/activo/update/"+activo._id;
    let body=activo;
    delete body._id;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });

    return this.http.put(url,body,{headers})
  }

  public editCama(cama:any){
    let url=this.servidor+"/cama/update/"+cama._id;
    let body=cama;
    delete body._id;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });

    return this.http.put(url,body,{headers})
  }

  public getActivos(){
    let url=this.servidor+"/activo/all";
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });

    return this.http.get(url,{headers})
  }

  public getCamas(){
    let url=this.servidor+"/cama/all";
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });

    return this.http.get(url,{headers})
  }

  public deleteActivo(id_activo:string, id_hotel:string){
    let url=this.servidor+"/activo/delete/"+id_hotel+"/"+id_activo;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });
    return this.http.delete(url,{headers})
  }

  public deleteCama(id_cama:string,idhotel:string){
    let url=this.servidor+"/cama/delete/"+idhotel+'/'+id_cama;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });
    return this.http.delete(url,{headers})
  }




}
