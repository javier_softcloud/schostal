import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  private servidor = environment.api_host;
  private token;
  private token_user;

  constructor(private http:HttpClient) {
    this.token=localStorage.getItem('token_admin')
    this.token_user=localStorage.getItem('token_user')

  }


  public signupUser(user_signup:Object){
    let url=this.servidor+"/cliente/add";
    let body = user_signup;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json'
    });
    return this.http.post(url,body,{headers})
  }

  public editSingleUser(cliente){
    let url=this.servidor+"/cliente/update/"+cliente._id;
    let body=cliente;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token_user
    });

    return this.http.put(url,body,{headers});
  }

  getSingleUser(){
    let url=this.servidor+"/cliente/me"

    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token_user
    });

    return this.http.get(url,{headers});
  }

  getUser(id){
    let url=this.servidor+"/cliente/"+id

    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });

    return this.http.get(url,{headers});
  }

  public getUsers(){
    let url=this.servidor+"/cliente/all";
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });

    return this.http.get(url,{headers});
  }

  public delete(id:number){
    let url=this.servidor+"/cliente/delete/"+id;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });

    return this.http.delete(url,{headers});
  }


  public edit(cliente:any){
    let url=this.servidor+"/cliente/update/"+cliente._id;
    let body=cliente;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });

    return this.http.put(url,body,{headers});
  }
}
