import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ReservacionService {

  is_on_reservarion:boolean;
  servidor:string= environment.api_host;
  token:string;
  token_admin:string;
  activated_reservation:any=null;

  constructor(private http:HttpClient) {
    this.token=localStorage.getItem('token_user')
    this.token_admin= localStorage.getItem('token_admin')
  }

  public searchReserva(reservacion:any){
    let url=this.servidor+"/reserva/search";
    let body=reservacion;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json'
    });
    return this.http.post(url,body,{headers})
  }

  public doReserva(reservacion:any){
    console.log('Reserva since provider', reservacion)
    let url=this.servidor+"/reserva/confirm";
    let body=reservacion;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });
    return this.http.post(url,body,{headers})
  }

  public getCCards(){
    let url=this.servidor+"/reserva/ccard";
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });
    return this.http.get(url,{headers})
  }

  public addCreditCard(card:any){
    let url=this.servidor+"/reserva/add/card";
    let body=card;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });

    return this.http.post(url,body,{headers})
  }

  public getReservacionUsuario(){
    let url=this.servidor+"/reserva/user";
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token
    });
    return this.http.get(url,{headers})
  }

  getAllReservaciones(){
    let url=this.servidor+"/reserva/all";
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization':this.token_admin
    });
    return this.http.get(url,{headers})
  }



}
