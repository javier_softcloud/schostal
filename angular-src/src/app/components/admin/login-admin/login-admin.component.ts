import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../../guards/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login-admin',
  templateUrl: './login-admin.component.html',
  styleUrls: ['./login-admin.component.css']
})
export class LoginAdminComponent implements OnInit {

  token:FormControl;
  admin_form:FormGroup;
  invalid_admin_flag:boolean=false;
  invalid_root_flag:boolean=false;
  @ViewChild('danger') error_modal:any;



  constructor(private _authService:AuthService,
              private router:Router) {
    this.token=new FormControl('',Validators.required);
    this.admin_form= new FormGroup({
      'email': new FormControl('',Validators.required),
      'password': new FormControl('',Validators.required)
    })
  }

  ngOnInit() {
  }

  sendTokenGoogleAuth(){
    console.log(this.token.value)
    this._authService.loginRoot(parseInt(this.token.value)).subscribe((data:any)=>{
      console.log(data)
      localStorage.setItem("token_admin",data.token);
      localStorage.setItem('role_admin',data.admin.role);
      this.router.navigate(['/admin','dashboard'])
    },err=>{
      if(err.status==401)
        this.invalid_root_flag=true;
      else if(err.status==0)
        this.error_modal.show();
    });
  }

  loginAdmin(){
    this._authService.loginAdmin(this.admin_form.value).subscribe((data:any)=>{
      localStorage.setItem('token_admin',data.token);
      localStorage.setItem('role_admin',data.admin.role);
      console.log(data);
      this.router.navigate(['/admin','dashboard']);
    },err=>{
      if(err.status==401)
        this.invalid_admin_flag=true;
      else if(err.status==0)
        this.error_modal.show();
    });
    }


}
