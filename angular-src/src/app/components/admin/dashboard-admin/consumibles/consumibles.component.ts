import {Component, OnInit, ViewChild} from '@angular/core';
import {UtileriaService} from "../../../../services/utileria.service";
import {HotelService} from "../../../../services/hotel.service";
import {ConsumiblesService} from "../../../../services/consumibles.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-consumibles',
  templateUrl: './consumibles.component.html',
  styleUrls: ['./consumibles.component.css']
})
export class ConsumiblesComponent implements OnInit {


  public sorted_consumible: boolean;
  public consumibles = [];
  public selected_consumible: any = {
    _id: null
  };
  public array_selected = [];
  public hoteles: any;
  public select_hoteles;
  public data_modal_flag: string;
  public consumible_form: FormGroup;
  public array_not_deleted: any = [];
  public array_deleted: any = [];
  @ViewChild('data_modal') data_modal: any;
  @ViewChild('success') success_modal: any;
  @ViewChild('danger') danger_modal: any;
  @ViewChild('modal_delete_array') modal_delete_array: any;
  @ViewChild('modal_sure_delete') modal_sure_delete: any;

  constructor(public _utilerias: UtileriaService,
              public _hotelService: HotelService,
              public _consumibleService:ConsumiblesService) {
  this.consumible_form = new FormGroup({
    _id: new FormControl(''),
    nombre: new FormControl('', Validators.required),
    precio: new FormControl('', Validators.required),
    idhotel: new FormControl('', Validators.required),
    descripcion: new FormControl('')
  })

  this.update();
  }

  ngOnInit() {
  }

  toggleCheckConsumible($event, index: number) {
    this.consumibles[index].check = $event.checked;
    if (this.selected_consumible._id)
      this.selected_consumible = {
        _id: null
      };
  }

  selectConsumible(consumible: any) {
    if (consumible == this.selected_consumible)
      this.selected_consumible = {
        _id: null
      }
    else
      this.selected_consumible = consumible;
  }

  openEditConsumibleModal() {
    this.consumible_form.reset();
    let consumibles: any[] = [];
    consumibles = this._utilerias.getCheked(this.consumibles);
    this.data_modal_flag = "editar";
    if (consumibles.length > 0) {
      this.consumible_form.get('_id').setValue(consumibles[0]._id);
      this.consumible_form.get('nombre').setValue(consumibles[0].nombre);
      this.consumible_form.get('precio').setValue(consumibles[0].precio);
      this.consumible_form.get('idhotel').setValue(consumibles[0].idhotel);
      this.consumible_form.get('cantidad').setValue(consumibles[0].cantidad);
      this.consumible_form.get('descripcion').setValue(consumibles[0].descripcion);

      this.data_modal.show();
    } else if (this.selected_consumible._id) {
      this.consumible_form.get('_id').setValue(this.selected_consumible._id);
      this.consumible_form.get('nombre').setValue(this.selected_consumible.nombre);
      this.consumible_form.get('precio').setValue(this.selected_consumible.precio);
      this.consumible_form.get('idhotel').setValue(this.selected_consumible.idhotel);
      this.consumible_form.get('descripcion').setValue(this.selected_consumible.descripcion);
      this.data_modal.show();
    }

    console.log(this.consumible_form.value);
  }

  confirmConsumibleDelete() {
    this.array_selected = [];
    for (let consumible of this.consumibles) {
      if (consumible.check)
        this.array_selected.push({_id: consumible._id, show: consumible.nombre, idhotel:consumible.idhotel})
    }
    if (this.array_selected.length == 0) {
      if (this.selected_consumible._id) {
        this.array_selected.push({_id: this.selected_consumible._id, show: this.selected_consumible.nombre, idhotel: this.selected_consumible.idhotel})
      }
    }
    this.modal_sure_delete.openModal();
  }

  openNewConsumibleModal() {
    this.data_modal_flag = "nuevo"
    this.consumible_form.reset();
    this.data_modal.show();
  }

  async deleteConsumible(delete_consumible: any) {
    this.modal_sure_delete.closeModal();
    this.array_deleted = [];
    this.array_not_deleted = [];
    for (let check of delete_consumible) {
      let activo_deleted = {
        _id: check._id,
        show: check.show,
        deleted: await this._consumibleService.deleteConsumible(check._id,check.idhotel).toPromise() ? true : false
      };
      if (activo_deleted.deleted)
        this.array_deleted.push(activo_deleted);
      else
        this.array_not_deleted.push(activo_deleted);
    }
    this.modal_delete_array.openModal();
    this.update()
  }

  async update() {
    let consumibleResponse: any;
    consumibleResponse = await this._consumibleService.getConsumible().toPromise();
    this.hoteles = await this._hotelService.getHoteles().toPromise();
    this.hoteles = this.hoteles.hoteles;
    this.consumibles = consumibleResponse.consumibles;
    this.consumibles = this._utilerias.addCheckItem(this.consumibles);
    this.select_hoteles = this._utilerias.covertToSelect(this.hoteles);
    console.log(this.hoteles,this.consumibles)
  }

  onCloseModal(){
    this.consumible_form.reset();
  }

  saveDataModal() {
    if (this.data_modal_flag == 'nuevo') {
      this.addConsumible();
    } else if (this.data_modal_flag == 'editar') {
      this.editaConsumible();
    }
  }

  addConsumible() {
    let newConsumible = this.consumible_form.value;
    delete newConsumible._id;
    console.log(newConsumible)
    this._consumibleService.addConsumible(newConsumible).subscribe(data => {
      this.data_modal.hide();
      this.success_modal.show();
      this.update();
    }, err => {
      this.danger_modal.show()
      console.log(err)
    })
  }

  editaConsumible(){
    let editActivo=this.consumible_form.value;
    console.log(editActivo);
    this._consumibleService.editConsumible(editActivo).subscribe(data=>{
      console.log(data);
      this.data_modal.hide()
      this.success_modal.show();
      this.update();
    },err=>{
      this.danger_modal.show();
      console.log(err)
    });
  }

}
