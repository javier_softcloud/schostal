import {Component, OnInit, ViewChild} from '@angular/core';
import {ClienteService} from "../../../../services/cliente.service";
import {UtileriaService} from "../../../../services/utileria.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {environment} from "../../../../../environments/environment";
import {Router} from "@angular/router";

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {
  public clientes:any;
  public sorted=false;
  public load_table:boolean=true;
  public table_view:boolean=true;
  public selected_user:any={
    _id:null
  };
  array_deleted:any[]=[];
  array_not_deleted:any[]=[];
  array_selected:any[]=[];
  public servidor;
  public edit_user_form:FormGroup;

  @ViewChild('edit_modal') edit_modal:any;
  @ViewChild('moda_delte') modal_delete:any;
  @ViewChild('moda_delte_array') modal_delete_array:any;
  @ViewChild('modal_sure_delete')modal_sure_delete:any;
  @ViewChild('success') success_modal:any;
  @ViewChild('danger') danger:any;
  constructor(private _userService:ClienteService,
              public _utilerias:UtileriaService,
              private router:Router) {
    this.servidor=environment.api_host
    this.array_deleted=[];
    this.updateUsers();
    this.edit_user_form=new FormGroup({
      nombre: new FormControl('',Validators.required),
      apPaterno:new FormControl('',Validators.required),
      apMaterno:new FormControl('',Validators.required),
      email: new FormControl('',[Validators.required,Validators.pattern(/^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/)]),
      _id: new FormControl('')
    })
  }

  ngOnInit() {
  }

  private async updateUsers(){
    this.load_table=true;
    this.clientes= await this._userService.getUsers().toPromise();
    this.clientes = this.clientes.usuarios
    console.log(this.clientes);
    if(!this.clientes){
      console.log("Try later")
      this.clientes=[];
    }
    this.clientes=this._utilerias.addCheckItem(this.clientes);
    this.load_table=false;
    console.log(this.clientes);
  }

  private toggleCheckUser($event,index:number){
    this.clientes[index].check=$event.checked;
  }

  selectUser(user:any){
    console.log(user)
    if(user==this.selected_user)
      this.selected_user={
      _id:null
      }
    else
      this.selected_user=user;
  }

  initSelected(){
    this.array_selected=[];
  }

  confirmDeletedUsers() {
    this.array_selected=[];
    for (let cliente of this.clientes) {
      if (cliente.check)
        this.array_selected.push({_id: cliente._id, show: cliente.nombre})
    }
    if(this.array_selected.length==0){
      if(this.selected_user._id) {
        this.array_selected.push({_id: this.selected_user._id, show: this.selected_user.nombre})
        this.modal_sure_delete.openModal();
      }
    }else{
      this.modal_sure_delete.openModal();
    }
  }

  openMoreInformation(){
    console.log(this.selected_user._id)
    if(this.selected_user != null)
      this.router.navigate(["/admin","dashboard","clientes",this.selected_user._id]);
  }

  openEditUserSection(){
    let array_selected:any[]=[];
    this.edit_user_form.reset();
    for (let cliente of this.clientes) {
      if (cliente.check)
        array_selected.push(cliente)
    }
    if(array_selected.length>0){
      this.edit_user_form.get('nombre').setValue(array_selected[0].nombre);
      this.edit_user_form.get('apPaterno').setValue(array_selected[0].apPaterno);
      this.edit_user_form.get('apMaterno').setValue(array_selected[0].apMaterno);
      this.edit_user_form.get('email').setValue(array_selected[0].email);
      this.edit_user_form.get('_id').setValue(array_selected[0]._id);
      this.edit_modal.show();
    }else if(this.selected_user._id){
      this.edit_user_form.get('nombre').setValue(this.selected_user.nombre);
      this.edit_user_form.get('email').setValue(this.selected_user.email);
      this.edit_user_form.get('apMaterno').setValue(this.selected_user.apMaterno);
      this.edit_user_form.get('apPaterno').setValue(this.selected_user.apPaterno);
      this.edit_user_form.get('_id').setValue(this.selected_user._id);
      this.edit_modal.show();
    }
  }

  editUser(){
    console.log(this.edit_user_form.value)
    this._userService.edit(this.edit_user_form.value).subscribe(data=>{
      this.updateUsers()
      this.edit_modal.hide()
      this.success_modal.show();
    },err=>{
      this.danger.show();
    });
  }

  async deleteUser(delete_users:any[]){
    this.load_table=true;
    this.modal_sure_delete.closeModal();
    this.array_deleted=[];
    this.array_not_deleted=[];
    for(let check of delete_users){
      let user_deleted={
        _id:check._id,
        show:check.show,
        deleted:await this._userService.delete(check._id).toPromise() ? true:false
      };
      if(user_deleted.deleted)
        this.array_deleted.push(user_deleted);
      else
        this.array_not_deleted.push(user_deleted);
    }
    this.modal_delete_array.openModal();
    this.updateUsers()
  }

}
