import { Component, OnInit } from '@angular/core';
import {ClienteService} from "../../../../../services/cliente.service";
import {ActivatedRoute, Router} from "@angular/router";
import {updateSourceFile} from "@angular/compiler-cli/src/transformers/node_emitter";
import {UtileriaService} from "../../../../../services/utileria.service";

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  isLoading = true;
  cliente:any={
    nombre:null,
    email:null
  };
  idCliente:string;

  constructor( private _userService: ClienteService,
               private route:ActivatedRoute,
               public _utils:UtileriaService) {
    this.idCliente = this.route.snapshot.params.id;
    this.updateComponent()
    console.log(this.idCliente)
  }

  async updateComponent(){
    this.cliente = await this._userService.getUser(this.idCliente).toPromise();
    console.log(this.cliente)
    this.isLoading = false;
  }


  ngOnInit() {
  }

}
