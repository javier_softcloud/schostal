import {Component, OnInit, ViewChild} from '@angular/core';
import {UtileriaService} from "../../../../services/utileria.service";
import {HotelService} from "../../../../services/hotel.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-hoteles',
  templateUrl: './hoteles.component.html',
  styleUrls: ['./hoteles.component.css']
})
export class HotelesComponent implements OnInit {

  public hoteles:any=[];
  public sorted:boolean=false;
  load_table:boolean=false;
  public selected_hotel:any={
    _id:null
  };
  public array_selected=[];
  public data_modal_flag:string;
  public array_deleted:any[]=[];
  public array_not_deleted:any[]=[];
  public data_form:FormGroup;
  public modal_flag_success:string;
  @ViewChild('data_modal') data_modal:any;
  @ViewChild('modal_delete_array') modal_delete_array:any;
  @ViewChild('modal_sure_delete')modal_sure_delete:any;
  @ViewChild('success') success:any;
  @ViewChild('danger') danger:any;


  constructor(public _utilerias:UtileriaService,
              public _HotelService:HotelService){
    this.data_form=new FormGroup({
      _id:new FormControl(''),
      nombre: new FormControl('',Validators.required),
      direccion: new FormControl('',Validators.required),
      tipo: new FormControl('',Validators.required),
      politicas_ninios: new FormControl('',Validators.required),
      max_age_children: new FormControl('',Validators.required)
    });
    this.update()
  }

  ngOnInit() {
  }


  private async update(){
    this.load_table=true;
    this.hoteles= await this._HotelService.getHoteles().toPromise();
    if(!this.hoteles){
      console.log("Try later")
      this.hoteles=[];
    }
    this.hoteles=this._utilerias.addCheckItem(this.hoteles.hoteles);
    this.load_table=false;
  }

  public toggleCheckHotel($event,index:number){
    this.hoteles[index].check=$event.checked;
  }

  selectHotel(hotel:any){
    if(hotel==this.selected_hotel)
      this.selected_hotel={
        _id:null
      }
    else
      this.selected_hotel=hotel;
  }

  initSelected(){
    this.array_selected=[];
  }

  editHotel(){
    this.modal_flag_success="editar";
    console.log(this.data_form.value);
    this._HotelService.editHotel(this.data_form.value).subscribe(data=>{
      console.log(data);
      this.data_modal.hide();
      this.success.show();
      this.update();
    },err=>{
      this.danger.show();
    });
  }

  async deleteHotel(delete_hotels:any[]){
    this.load_table=true;
    this.modal_sure_delete.closeModal();
    this.array_deleted=[];
    this.array_not_deleted=[];
    for(let check of delete_hotels){
      let hotel_deleted={
        _id:check._id,
        show:check.show,
        deleted:await this._HotelService.deleteHotel(check._id).toPromise() ? true:false
      };
      if(hotel_deleted)
        this.array_deleted.push(hotel_deleted);
      else
        this.array_not_deleted.push(hotel_deleted);
    }
    this.modal_delete_array.openModal();
    this.update()
  }

  addHotel(){
    let newHotel=this.data_form.value;
    this.modal_flag_success="nuevo";
    this._HotelService.addHotel(newHotel).subscribe(data=>{
      this.data_modal.hide();
      this.success.show();
      this.update();
      this.data_form.reset();
    },err=>{
      this.data_modal.hide();
      this.danger.show()
    })
  }

  openNewHotelModal(){
    this.data_modal.show();
    this.data_modal_flag="nuevo"

  }

  openEditAdminModal(){
    let hoteles:any[]=[];
    hoteles=this._utilerias.getCheked(this.hoteles);
    this.data_modal_flag="editar";
    if(hoteles.length>0){
      this.data_form.get('_id').setValue(hoteles[0]._id);
      this.data_form.get('nombre').setValue(hoteles[0].nombre);
      this.data_form.get('direccion').setValue(hoteles[0].direccion);
      this.data_form.get('tipo').setValue(hoteles[0].direccion);
      this.data_form.get('politicas_ninios').setValue(hoteles[0].politicas_ninios);
      this.data_form.get('max_age_children').setValue(hoteles[0].max_age_children);
      this.data_modal.show();
    }else if(this.selected_hotel._id){
      this.data_form.get('_id').setValue(this.selected_hotel._id);
      this.data_form.get('nombre').setValue(this.selected_hotel.nombre);
      this.data_form.get('direccion').setValue(this.selected_hotel.direccion);
      this.data_form.get('tipo').setValue(this.selected_hotel.direccion);
      this.data_form.get('politicas_ninios').setValue(this.selected_hotel.politicas_ninios);
      this.data_form.get('max_age_children').setValue(this.selected_hotel.max_age_children);
      this.data_modal.show();
    }
  }

  confirmHotelDelete(){
    this.array_selected=[];
    for (let hotel of this.hoteles) {
      if (hotel.check)
        this.array_selected.push({_id: hotel._id, show: hotel.nombre})
    }
    if(this.array_selected.length==0){
      if(this.selected_hotel._id) {
        this.array_selected.push({_id: this.selected_hotel._id, show: this.selected_hotel.nombre})
        this.modal_sure_delete.openModal();
      }
    }else{
      this.modal_sure_delete.openModal();
    }
  }


}
