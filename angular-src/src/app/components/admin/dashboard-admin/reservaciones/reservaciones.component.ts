import { Component, OnInit } from '@angular/core';
import {UtileriaService} from "../../../../services/utileria.service";
import {ReservacionService} from "../../../../services/reservacion.service";
import {ClienteService} from "../../../../services/cliente.service";
import {HotelService} from "../../../../services/hotel.service";

@Component({
  selector: 'app-reservaciones',
  templateUrl: './reservaciones.component.html',
  styleUrls: ['./reservaciones.component.css']
})
export class ReservacionesComponent implements OnInit {

  public reservaciones:any=[];
  public sorted:any;
  public hoteles:any=[];
  public selected_reservacion:any={
    _id:null
  };
  public usuarios:any=[];

  constructor(public _utilerias:UtileriaService,
              public _reservacionService:ReservacionService,
              public _userService:ClienteService,
              public _hotelService:HotelService) {
    this._reservacionService.getAllReservaciones().subscribe((reservaciones:any)=>{
      this.reservaciones=reservaciones.reservaciones;
      console.log('Reservaciones',this.reservaciones);
    })


    this._userService.getUsers().subscribe((usuarios:any)=>{
      this.usuarios=usuarios.usuarios;
    })

    this._hotelService.getHoteles().subscribe((hoteles:any)=>{
      this.hoteles=hoteles.hoteles;
    })



  }

  ngOnInit() {
  }

  selectReservacion(reservacion:any){
    if(reservacion==this.selected_reservacion)
      this.selected_reservacion={
        _id:null
      }
    else
      this.selected_reservacion=reservacion;

    console.log(this.selected_reservacion)

  }

  public toggleCheckReservacion($event,index:number){
    this.reservaciones[index].check=$event.checked;
  }

  getUserName(iduser:string){
    return this._utilerias.getPropertyById('nombre',this.usuarios,iduser) +" "+this._utilerias.getPropertyById('apPaterno',this.usuarios,iduser)
  }

  getHotelName(idhotel){
    return this._utilerias.getPropertyById('nombre',this.hoteles,idhotel)
  }
}
