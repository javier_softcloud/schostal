import {Component, OnInit, ViewChild} from '@angular/core';
import {UtileriaService} from "../../../../services/utileria.service";
import {HotelesComponent} from "../hoteles/hoteles.component";
import {HotelService} from "../../../../services/hotel.service";
import {ActivoCamaService} from "../../../../services/activo-cama.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-activos-camas',
  templateUrl: './activos-camas.component.html',
  styleUrls: ['./activos-camas.component.css']
})
export class ActivosCamasComponent implements OnInit {

  public sorted_activo: boolean;
  public sorted_camas: boolean;
  public activos = [];
  public camas:any;
  public selected_activo: any = {
    _id: null
  };
  public selected_cama:any = {
    _id: null
  };
  public select_tipo:any[]=[
    {value:"Matrimonial",label:"Matrimonial"},
    {value:"King Size",label:"King Size"},
    {value:"Queen",label:"Queen Size"},
    {value:"Individual",label:"Individual"},
  ];
  public  type_modal_open:string;
  public array_selected = [];
  public hoteles: any;
  public select_hoteles;
  public data_modal_flag: string;
  public activo_form: FormGroup;
  public cama_form: FormGroup;
  public array_not_deleted: any = [];
  public array_deleted: any = [];
  @ViewChild('data_modal') data_modal: any;
  @ViewChild('success') success_modal: any;
  @ViewChild('danger') danger_modal: any;
  @ViewChild('modal_delete_array') modal_delete_array: any;
  @ViewChild('modal_sure_delete') modal_sure_delete: any;

  constructor(public _utilerias: UtileriaService,
              public _hotelService: HotelService,
              public _activoService: ActivoCamaService) {
    this.activo_form = new FormGroup({
      _id: new FormControl(''),
      nombre: new FormControl('', Validators.required),
      precio: new FormControl('', Validators.required),
      idhotel: new FormControl('', Validators.required),
      descripcion: new FormControl(''),
      cantidad: new FormControl('', Validators.required)
    })
    this.cama_form= new FormGroup({
      _id: new FormControl('', Validators.required),
      nombre: new FormControl('', Validators.required),
      ocupacion: new FormControl('', Validators.required),
      tipo: new FormControl('', Validators.required),
      idhotel:new FormControl('', Validators.required)
    })
    this.update()
  }

  ngOnInit() {
  }

  async update() {
    let activosResponse: any;
    let camasResponse:any;
    activosResponse = await this._activoService.getActivos().toPromise();
    this.hoteles = await this._hotelService.getHoteles().toPromise();
    this.hoteles = this.hoteles.hoteles;
    this.activos = activosResponse.activos;
    this.activos = this._utilerias.addCheckItem(this.activos);
    this.select_hoteles = this._utilerias.covertToSelect(this.hoteles);
    camasResponse= await this._activoService.getCamas().toPromise();
    console.log(this.activos)
    this.camas=camasResponse.camas;
  }

  openEditActivosModal() {
    this.activo_form.reset()
    this.type_modal_open="activo";
    this.activo_form.reset();
    let activos: any[] = [];
    activos = this._utilerias.getCheked(this.activos);
    this.data_modal_flag = "editar";
    if (activos.length > 0) {
      this.activo_form.get('_id').setValue(activos[0]._id);
      this.activo_form.get('nombre').setValue(activos[0].nombre);
      this.activo_form.get('precio').setValue(activos[0].precio);
      this.activo_form.get('idhotel').setValue(activos[0].idhotel);
      this.activo_form.get('cantidad').setValue(activos[0].cantidad);
      this.activo_form.get('descripcion').setValue(activos[0].descripcion);

      this.data_modal.show();
    } else if (this.selected_activo._id) {
      this.activo_form.get('_id').setValue(this.selected_activo._id);
      this.activo_form.get('nombre').setValue(this.selected_activo.nombre);
      this.activo_form.get('precio').setValue(this.selected_activo.precio);
      this.activo_form.get('idhotel').setValue(this.selected_activo.idhotel);
      this.activo_form.get('cantidad').setValue(this.selected_activo.cantidad);
      this.activo_form.get('descripcion').setValue(this.selected_activo.descripcion);
      this.data_modal.show();
    }

    console.log(this.activo_form.value);
  }

  openEditCamaModal() {
    this.type_modal_open="cama";
    this.cama_form.reset();
    let camas: any[] = [];
    camas = this._utilerias.getCheked(this.camas);
    this.data_modal_flag = "editar";
    if (camas.length > 0) {
      this.cama_form.get('_id').setValue(camas[0]._id);
      this.cama_form.get('nombre').setValue(camas[0].nombre);
      this.cama_form.get('idhotel').setValue(camas[0].idhotel);
      this.cama_form.get('ocupacion').setValue(camas[0].ocupacion);
      this.cama_form.get('tipo').setValue(camas[0].tipo);

      this.data_modal.show();
    } else if (this.selected_cama._id) {
      this.cama_form.get('_id').setValue(this.selected_cama._id);
      this.cama_form.get('nombre').setValue(this.selected_cama.nombre);
      this.cama_form.get('idhotel').setValue(this.selected_cama.idhotel);
      this.cama_form.get('ocupacion').setValue(this.selected_cama.ocupacion);
      this.cama_form.get('tipo').setValue(this.selected_cama.tipo);
      this.data_modal.show();
    }

  }

  confirmActivosDelete() {
    this.array_selected = [];
    this.type_modal_open='activo'
    for (let activo of this.activos) {
      if (activo.check)
        this.array_selected.push({_id: activo._id, show: activo.nombre, idhotel:activo.idhotel})
    }
    if (this.array_selected.length == 0) {
      if (this.selected_activo._id) {
        this.array_selected.push({_id: this.selected_activo._id, show: this.selected_activo.nombre, idhotel:this.selected_activo.idhotel})
        this.modal_sure_delete.openModal();
      }
    } else {
      this.modal_sure_delete.openModal();
    }
  }

  openNewActivosModal() {
    this.data_modal_flag = "nuevo"
    this.type_modal_open="activo";
    this.activo_form.reset();
    this.data_modal.show();
  }

  openNewCamaModal() {
    this.type_modal_open="cama";
    this.data_modal_flag = "nuevo"
    this.activo_form.reset();
    this.data_modal.show();
  }

  selectActivo(activo: any) {
    if (activo == this.selected_activo)
      this.selected_activo = {
        _id: null
      }
    else
      this.selected_activo = activo;

    console.log(this.selected_activo)
  }

  selectCama(cama: any) {
    if (cama == this.selected_cama)
      this.selected_cama = {
        _id: null
      }
    else
      this.selected_cama = cama;
  }

  toggleCheckActivo($event, index: number) {
    this.activos[index].check = $event.checked;
    if (this.selected_activo._id)
      this.selected_activo = {
        _id: null
      };
  }

  toggleCheckCama($event, index: number) {
    this.camas[index].check = $event.checked;
    if (this.selected_cama._id)
      this.selected_cama = {
        _id: null
      };
  }


  saveDataModal() {
    if (this.data_modal_flag == 'nuevo') {
      this.addActivo();
    } else if (this.data_modal_flag == 'editar') {
      this.editaActivo();
    }
  }

  saveDataModalCama() {
    if (this.data_modal_flag == 'nuevo') {
      this.addCama();
    } else if (this.data_modal_flag == 'editar') {
      //this.editaCama();
    }
  }


  addActivo() {
    let newActivo = this.activo_form.value;
    delete newActivo._id;
    console.log(newActivo)
    this._activoService.addActivo(newActivo).subscribe(data => {
      this.data_modal.hide();
      this.success_modal.show();
      this.update();
    }, err => {
      this.danger_modal.show()
      console.log(err)
    })
  }

  addCama() {
    let newCama = this.cama_form.value;
    delete newCama._id;
    console.log(newCama)
    this._activoService.addCama(newCama).subscribe(data => {
      console.log(data)
      this.data_modal.hide();
      this.success_modal.show();
      this.update();
    }, err => {
      this.danger_modal.show()
      console.log(err)
    })
  }

  confirmUsersDelete() {
    this.array_selected = [];
    for (let activo of this.activos) {
      if (activo.check)
        this.array_selected.push({_id: activo._id, show: activo.nombre})
    }
    if (this.array_selected.length == 0) {
      if (this.selected_activo._id) {
        this.array_selected.push({_id: this.selected_activo._id, show: this.selected_activo.nombre})
        this.modal_sure_delete.openModal();
      }
    } else {
      this.modal_sure_delete.openModal();
    }
  }

  async deleteActivo(delete_activo: any) {
    console.log("A eliminar: ",delete_activo);
    this.modal_sure_delete.closeModal();
    this.array_deleted = [];
    this.array_not_deleted = [];
    for (let check of delete_activo) {
      let activo_deleted = {
        _id: check._id,
        show: check.show,
        deleted: await this._activoService.deleteActivo(check._id,check.idhotel).toPromise() ? true : false
      };
      if (activo_deleted.deleted)
        this.array_deleted.push(activo_deleted);
      else
        this.array_not_deleted.push(activo_deleted);
    }
    this.modal_delete_array.openModal();
    this.update()
  }

  async deleteCama(delete_cama: any) {
    this.modal_sure_delete.closeModal();
    this.array_deleted = [];
    this.array_not_deleted = [];
    for (let check of delete_cama) {
      let cama_deleted = {
        _id: check._id,
        show: check.show,
        deleted: await this._activoService.deleteCama(check._id,check.idhotel).toPromise() ? true : false
      };
      if (cama_deleted.deleted)
        this.array_deleted.push(cama_deleted);
      else
        this.array_not_deleted.push(cama_deleted);
    }
    this.modal_delete_array.openModal();
    this.update()
  }

  editaActivo(){
    let editActivo=this.activo_form.value;
    console.log(editActivo);
    this._activoService.editActivo(editActivo).subscribe(data=>{
      console.log(data);
      this.data_modal.hide()
      this.success_modal.show();
      this.update();
    },err=>{
      this.danger_modal.show();
      console.log(err)
    });
  }

  editaCama(){
    let editCama=this.cama_form.value;
    console.log(editCama);
    this._activoService.editCama(editCama).subscribe(data=>{
      console.log(data);
      this.data_modal.hide()
      this.success_modal.show();
      this.update();
    },err=>{
      this.danger_modal.show();
      console.log(err)
    });
  }

  confirmCamaDelete() {
    this.type_modal_open='cama';
    this.array_selected = [];
    for (let cama of this.camas) {
      if (cama.check)
        this.array_selected.push({_id: cama._id, show: cama.nombre, idhotel:cama.idhotel})
    }
    if (this.array_selected.length == 0) {
      if (this.selected_cama._id) {
        this.array_selected.push({_id: this.selected_cama._id, show: this.selected_cama.nombre, idhotel: this.selected_cama.idhotel})
      }
    }
    this.modal_sure_delete.openModal();
  }

  selectFunctionDelete(array_delete:any[]){
    console.log("Pase a función delete", array_delete)
    if(this.type_modal_open=='activo')
      this.deleteActivo(array_delete);
    else
      this.deleteCama(array_delete)
  }

  onCloseModal(){
    this.activo_form.reset();
    this.cama_form.reset()
  }
}


