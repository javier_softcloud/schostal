import {Component, OnInit, ViewChild} from '@angular/core';
import {HabitacionService} from "../../../../services/habitacion.service";
import {HotelService} from "../../../../services/hotel.service";
import {UtileriaService} from "../../../../services/utileria.service";
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivoCamaService} from "../../../../services/activo-cama.service";
import {DndDropEvent} from "ngx-drag-drop";
import {TemporadaService} from "../../../../services/temporada.service";
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-habitaciones',
  templateUrl: './habitaciones.component.html',
  styleUrls: ['./habitaciones.component.css']
})
export class HabitacionesComponent implements OnInit {

  public habitaciones: any;
  public hoteles: any;
  public select_hoteles: any;
  public select_camas: any;
  public selected_habitacion: any = {
    _id: null
  }
  public select_edit_temporada={
    _id:null
  };
  public select_edit_revenue={
    _id:null
  };
  public on_edit_temporada:boolean=false;
  public on_edit_revenue:boolean=false;
  public revenues_selected:any=[];
  public temporadas_selected:any=[];
  public temporadas:any=[];
  public revenues:any=[];
  habitacion_form: FormGroup;
  public data_modal_flag: string;
  public sorted: boolean;
  public cantidad_control: FormControl;
  public cama_id: FormControl;
  public camas: any = [];
  public precio_temporada:FormControl;
  public precio_revenue:FormControl;
  public camas_aux: any = [];
  public camas_asignadas: any[] = [];
  public cama_search_input: FormControl;
  public array_selected: any = [];
  public array_deleted: any = [];
  public array_not_deleted: any = [];
  @ViewChild('data_modal') data_modal: any;
  @ViewChild('success') success_modal: any;
  @ViewChild('danger') danger_modal: any;
  @ViewChild('modal_delete_array') modal_delete_array: any;
  @ViewChild('modal_sure_delete') modal_sure_delete: any;

  constructor(public _habitacionService: HabitacionService,
              public _hotelesService: HotelService,
              public _utilerias: UtileriaService,
              public formBuilder: FormBuilder,
              public _activoService: ActivoCamaService,
              public _temporadasService:TemporadaService) {
    this.cama_search_input = new FormControl('');
    this.cama_id = new FormControl('');
    this.initDataForm()
    this.update();
    this.precio_temporada= new FormControl('',Validators.required)
    this.precio_revenue= new FormControl('',Validators.required)
  }

  ngOnInit() {
  }

  public async update() {
    let habitacionesResponse: any;
    habitacionesResponse = await this._habitacionService.getHabitaciones().toPromise();
    this.hoteles = await this._hotelesService.getHoteles().toPromise();
    this.hoteles = this.hoteles.hoteles;
    this.habitaciones = habitacionesResponse.habitaciones;
    this.habitaciones = this._utilerias.addCheckItem(this.habitaciones);
    console.log(this.habitaciones)
    this.select_hoteles = this._utilerias.covertToSelect(this.hoteles);
    this.camas = await this._activoService.getCamas().toPromise();
    this.camas = this.camas.camas;
    this.cantidad_control = new FormControl('', [this.camasQuantityValidator.bind({
      camas: this.camas,
      cama_id: this.cama_id
    })]);
    this.select_camas = this._utilerias.covertToSelect(this.camas);
    console.log(this.camas)
  }

  selectHabitacion(habitacion: any) {
    if (habitacion == this.selected_habitacion)
      this.selected_habitacion = {
        _id: null
      }
    else
      this.selected_habitacion = habitacion;
  }

  toggleCheckAdmin($event, index: number) {
    this.habitaciones[index].check = $event.checked;
    if (this.selected_habitacion._id)
      this.selected_habitacion = {
        _id: null
      };
  }

  openNewHabitacionModal() {
    this.camas_aux = this.camas;
    this.camas_asignadas = [];
    this.data_modal_flag = "nuevo"
    this.habitacion_form.reset();
    this.data_modal.show();
  }

  getPrecios(hotel_id:string){
    this._temporadasService.getTemporada(hotel_id).subscribe((data:any)=>{
      this.temporadas=data.temporadas;
    })
    this._temporadasService.getRevenue(hotel_id).subscribe((data:any)=>{
      this.revenues=data.revs;
    })
  }

  onSelectHotel($event){
    let hotel_id=$event.value;
    this.getPrecios(hotel_id)
  }

  addHabitacion() {
    let newHabitacion = this.habitacion_form.value;
    newHabitacion = this.addCamasToForm(newHabitacion);
    delete newHabitacion._id;
    newHabitacion.ocupacion['tarifas']=this.temporadas_selected;
    newHabitacion.ocupacion['revenues']=this.revenues_selected;
    this._habitacionService.addHabitacion(newHabitacion).subscribe(data => {
      this.data_modal.hide();
      this.success_modal.show();
      this.update();
    }, err => {
      this.danger_modal.show()
    })
    this.temporadas_selected=[];
  }

  editTarifa(temporada:any){
    for(let index_temporada in this.temporadas_selected){
      if(this.temporadas_selected[index_temporada].temporada._id==temporada._id){
        this.temporadas_selected[index_temporada].precio=this.precio_temporada.value;
      }
    }
    this.select_edit_temporada={
      _id:null
    }
    this.on_edit_temporada=false;
  }

  editRevenue(revenue:any){
    for(let index_revenue in this.revenues_selected){
      if(this.revenues_selected[index_revenue].revenue._id==revenue._id){
        this.revenues_selected[index_revenue].precio=this.precio_revenue.value;
      }
    }
    this.select_edit_revenue={
      _id:null
    }
    this.on_edit_revenue=false;
  }

  deleteAsignadas(array_camas: any) {
    for (let index_cama in array_camas)
      if (array_camas[index_cama].asignada)
        array_camas.splice(parseInt(index_cama), 1)
    return array_camas;
  }

  addCamasToForm(inputForm: any) {
    inputForm['ocupacion'] = {
      camas: []
    }
    for (let cama of this.camas_asignadas) {
      inputForm.ocupacion.camas.push(cama._id)
    }
    return inputForm;
  }

  saveDataModal() {
    if (this.data_modal_flag == 'nuevo') {
      this.addHabitacion();
    } else if (this.data_modal_flag == 'editar') {
      //this.editAdmin();
    }
  }

  initDataForm() {
    this.habitacion_form = new FormGroup({
      _id: new FormControl(''),
      nombre: new FormControl('', Validators.required),
      ocupacion_max: new FormControl('', Validators.required),
      descripcion: new FormControl('', Validators.required),
      max_children: new FormControl('', Validators.required),
      idhotel: new FormControl('', Validators.required),
      precioBase: new FormControl('', Validators.required)
    })
  }

  selectEditTemporada(item_selected:any){
    this.on_edit_temporada=true;
    this.select_edit_temporada=item_selected;
    this.precio_temporada.setValue(this.getPrecioById(item_selected._id))
  }

  selectEditRevenue(item_selected:any){
    this.precio_revenue.setValue(this.getPrecioById(item_selected._id))
    this.on_edit_revenue=true;
    this.select_edit_revenue=item_selected;
  }

  cancelEditTemporada(){
    this.on_edit_temporada=false;
    this.select_edit_temporada={
      _id:null
    };
  }

  cancelEditRevenue(){
    this.on_edit_revenue=false;
    this.select_edit_revenue={
      _id:null
    };
  }

  openEditHabitacionesModal() {
    this.camas_aux = this.camas;
    let habitaciones: any[] = [];
    habitaciones = this._utilerias.getCheked(this.habitaciones);
    this.initDataForm();
    this.data_modal_flag = "editar";
    if (habitaciones.length > 0) {
      this.camas_aux = this.camas.filter((cama) => cama.idhotel === habitaciones[0].idhotel);
      console.log(this.camas_aux);
      this.habitacion_form.get('_id').setValue(habitaciones[0]._id);
      this.habitacion_form.get('nombre').setValue(habitaciones[0].nombre);
      this.habitacion_form.get('ocupacion_max').setValue(habitaciones[0].ocupacion_max);
      this.habitacion_form.get('descripcion').setValue(habitaciones[0].descripcion);
      this.habitacion_form.get('max_children').setValue(habitaciones[0].max_children);
      this.habitacion_form.get('precioBase').setValue(habitaciones[0].precioBase);
      this.habitacion_form.get('idhotel').setValue(habitaciones[0].idhotel);
      this.getPrecios(habitaciones[0].idhotel)
      this.addCamasToCamasAsignadas(this.habitaciones[0].ocupacion.camas);
      this.revenues_selected=this.habitaciones[0].ocupacion.revenues;
      this.temporadas_selected=this.habitaciones[0].ocupacion.tarifas;
      this.data_modal.show();
    } else if (this.selected_habitacion._id) {
      this.habitacion_form.get('_id').setValue(this.selected_habitacion._id);
      this.habitacion_form.get('nombre').setValue(this.selected_habitacion.nombre);
      this.habitacion_form.get('ocupacion_max').setValue(this.selected_habitacion.ocupacion_max);
      this.habitacion_form.get('descripcion').setValue(this.selected_habitacion.descripcion);
      this.habitacion_form.get('max_children').setValue(this.selected_habitacion.max_children);
      this.habitacion_form.get('precioBase').setValue(this.selected_habitacion.precioBase);
      this.habitacion_form.get('idhotel').setValue(this.selected_habitacion.idhotel);
      this.getPrecios(this.selected_habitacion.idhotel)
      this.addCamasToCamasAsignadas(this.selected_habitacion.ocupacion.camas);
      this.revenues_selected=this.selected_habitacion.ocupacion.revenues;
      this.temporadas_selected=this.selected_habitacion.ocupacion.tarifas;
      this.data_modal.show();
    }
    console.log(this.camas_asignadas)
  }

  ifTarifaExist(tarifa:any){
    for(let tarifa_select of  this.temporadas_selected){
      if(tarifa_select.temporada._id==tarifa._id)
        return true
    }

    return false;
  }

  ifRevenueExist(revenue:any){
    for(let revenue_select of  this.revenues_selected){
      if(revenue_select.revenue._id==revenue._id)
        return true
    }

    return false;
  }

  addTarifaToArray(temporada:any){
    this.temporadas_selected.push({
      precio:this.precio_temporada.value,
      temporada:temporada
    })
    this.on_edit_temporada=false;
    this.select_edit_temporada={
      _id:null
    }
    this.precio_temporada.reset()
  }

  addRevenueToArray(revenue:any){
    this.revenues_selected.push({
      precio:this.precio_revenue.value,
      revenue:revenue
    })
    this.on_edit_revenue=false;
    this.select_edit_revenue={
      _id:null
    }
    this.precio_revenue.reset()
    console.log(this.revenues_selected)
  }

  getPrecioById(revenue:any){
    for(let tarifa_select of  this.temporadas_selected){
      if(tarifa_select.temporada._id==revenue._id)
        return tarifa_select.precio
    }

    for(let revenue_select of  this.revenues_selected){
      if(revenue_select.revenue._id==revenue._id)
        return revenue_select.precio
    }

    return "";
  }

  deleteTarifa(temporada){
    for(let tarifa_index in  this.temporadas_selected){
      if(this.temporadas_selected[tarifa_index].temporada._id==temporada._id)
        this.temporadas_selected.splice(tarifa_index,1);
    }
  }

  deleteRevenue(revenue){
    for(let revenue_index in  this.temporadas_selected){
      if(this.temporadas_selected[revenue_index].temporada._id==revenue._id)
        this.temporadas_selected.splice(revenue_index,1);
    }
  }

  addCamasToCamasAsignadas(camas: any[]) {
    this.camas_asignadas=[];
    for (let cama of camas)
      this.camas_asignadas.push(this.getCamaObjectById(cama._id));
  }

  getCamaObjectById(id: string) {
    for (let cama of this.camas) {
      if (cama._id == id) {
        console.log(cama)
        return cama;
      }
    }
  }

  confirmHabitacionDelete() {
    this.array_selected = [];
    for (let habitacion of this.habitaciones) {
      if (habitacion.check)
        this.array_selected.push({_id: habitacion._id, show: habitacion.nombre, idhotel: habitacion.idhotel})
    }
    if (this.array_selected.length == 0) {
      if (this.selected_habitacion._id) {
        this.array_selected.push({
          _id: this.selected_habitacion._id,
          show: this.selected_habitacion.nombre,
          idhotel: this.selected_habitacion.idhotel
        })
        this.modal_sure_delete.openModal();
      }
    } else {
      this.modal_sure_delete.openModal();
    }
  }

  editHabitacion() {
    let edit_hab=this.habitacion_form.value;
    console.log(this.temporadas_selected,this.revenues_selected,this.camas_asignadas)
    edit_hab['ocupacion']={
      tarifas:this.temporadas_selected,
      revenues:this.revenues_selected,
      camas:this.camas_asignadas
    };

    console.log(edit_hab)
    this._habitacionService.editHabitaciones(edit_hab).subscribe(data => {
      this.data_modal.hide();
      this.success_modal.show();
    })
  }

  changeAsignadaOfAux(id){
    for(let index_cama in this.camas_aux){
      if(id==this.camas_aux[index_cama]._id)
        this.camas_aux[index_cama].asignada=false;
    }
  }

  async deleteHabitacion(delete_habitaciones: any) {
    this.modal_sure_delete.closeModal();
    this.array_deleted = [];
    this.array_not_deleted = [];
    console.log(delete_habitaciones)
    for (let check of delete_habitaciones) {
      let habitacion_deleted = {
        _id: check._id,
        show: check.show,
        deleted: await this._habitacionService.deleteHabitacion(check._id).toPromise() ? true : false
      };
      if (habitacion_deleted.deleted)
        this.array_deleted.push(habitacion_deleted);
      else
        this.array_not_deleted.push(habitacion_deleted);
    }
    this.modal_delete_array.openModal();
    this.update()
  }

  camasQuantityValidator(control: FormControl) {
    let camas: any = this.camas;
    let cama_id: any = this.cama_id;

    if (control.value) {
      for (let item_cama of camas) {
        if (item_cama._id == cama_id.value && item_cama.cantidad >= control.value) {
          return null;
        }
      }
      return {
        camasQuantity: {
          dosponible: 'Disponible'
        }
      }
    } else {
    }
  }

  removeCama(cama: any) {
    let camas = this.habitacion_form.get('camas_disponibles') as FormArray;
    for (let $num in camas.controls) {
      if (camas[$num] == cama.cama) {

      }
    }
  }


  draggable = "Hola soy un dato";

  onDragStart(event: DragEvent) {

    console.log("drag started", event);
  }

  onDragEnd(event: DragEvent) {

    console.log("drag ended", event);
  }

  onDraggableCopied(event: DragEvent) {

    console.log("draggable copied", JSON.stringify(event, null, 2));
  }

  onDraggableLinked(event: DragEvent) {

    console.log("draggable linked", JSON.stringify(event, null, 2));
  }

  onDraggableMoved(event: DragEvent) {

    console.log("Se ha movido", JSON.stringify(event, null, 2));
  }

  onDragCanceled(event: DragEvent) {

    console.log("drag cancelled", JSON.stringify(event, null, 2));
  }

  onDragover(event: DragEvent) {

    console.log("dragover", JSON.stringify(event, null, 2));
  }

  deleteOfCamasAsigndas(event: DndDropEvent) {
    let item = event.data;
    if (this.ifItemExitOnList(item._id, this.camas_aux) || item.asignada) {
      this.popById(item._id, this.camas_asignadas);
      this.changeAsignadaOfAux(item._id);
      if (!item.asignada)
        this.camas_aux.push(item);
    }
  }

  onDrop(event: DndDropEvent) {
    let item = event.data;
    if (this.ifItemExitOnList(item._id, this.camas_asignadas)) {
      this.camas_asignadas.push(item);
      this.popById(item._id, this.camas);
      this.popById(item._id, this.camas_aux);
    }
  }

  popById(id, array: any[]) {
    for (let index in array) {
      if (id == array[index]._id)
        array.splice(parseInt(index), 1)
    }
  }

  ifItemExitOnList(id, array: any[]) {
    for (let index in array) {
      if (array[index]._id == id)
        return false;
    }

    return true;
  }


  searchCama(key: string) {
    console.log(key);
    if (key == "" || key == " " || null) {
      this.camas_aux = this.camas;
    } else {
      this.camas_aux = [];
      for (let cama of this.camas) {
        if (cama.nombre.includes(key) && this.ifItemExitOnList(cama._id, this.camas_aux))
          this.camas_aux.push(cama)
      }

      for (let cama of this.camas) {
        if (cama.tipo.includes(key) && this.ifItemExitOnList(cama._id, this.camas_aux))
          this.camas_aux.push(cama)
      }

      for (let cama of this.camas) {
        if ((cama.ocuacion == parseInt(key)) && this.ifItemExitOnList(cama._id, this.camas_aux))
          this.camas_aux.push(cama)
      }

    }

  }

  onCloseDataModal(){
    this.update();
    this.camas_asignadas=[]
    this.revenues_selected=[];
    this.temporadas_selected=[];
  }
}

