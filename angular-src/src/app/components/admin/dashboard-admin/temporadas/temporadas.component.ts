import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {IMyOptions} from "ng-uikit-pro-standard";
import {TemporadaService} from "../../../../services/temporada.service";
import {HotelService} from "../../../../services/hotel.service";
import {UtileriaService} from "../../../../services/utileria.service";
import * as moment from 'moment';
@Component({
  selector: 'app-temporadas',
  templateUrl: './temporadas.component.html',
  styleUrls: ['./temporadas.component.css']
})
export class TemporadasComponent implements OnInit {

  @ViewChild('success') success_modal:any;
  @ViewChild('modal_delete') delete_modal:any;
  @ViewChild('modal_success_delete') modal_success_delete:any;
  @ViewChild('danger') connection_error:any;
  @ViewChild('moda_error_fechas') fecha_error:any;

  public temporada_form:FormGroup;
  public temporadas:any=[];
  public revenues:any=[];
  public hoteles:any=[];
  public type_on:string;
  public action_on:string;
  public fechaInicio:FormControl;
  public fechaFinal:FormControl;
  public nombre: FormControl;
  public hotel:FormControl;
  public temporada_on={
    nombre:"",
    _id:null
  };
  public blackouts:any=[];
  public id_on_edit:string;
  public on_edit:boolean=false;
  public select_hoteles;
  public date_options: IMyOptions={
    dateFormat:"mm/dd/yyyy",
    closeAfterSelect:true,
    disableHeaderButtons:false,
    alignSelectorRight:true
  };
  public today;

  constructor(public _temporadaService:TemporadaService,
              public _hotelService:HotelService,
              public _utileriasService:UtileriaService) {
    this.fechaFinal= new FormControl('');
    this.fechaInicio= new FormControl('');
    this.nombre= new FormControl('')
    this.hotel=new FormControl('');
    this.temporada_form= new FormGroup({
      nombre: new FormControl('', Validators.required),
      fechaInicio: new FormControl('', Validators.required),
      fechaFinal: new FormControl('', Validators.required),
      idhotel: new FormControl('')
    })

    this.today=new Date();
    this.update()
  }

  ngOnInit() {
  }

  update(){
    this._temporadaService.getTemporada().subscribe((data:any)=>{
      this.temporadas=data.temporadas;
      console.log(this.temporadas)
    })

    this._temporadaService.getRevenue().subscribe((data:any)=>{
      this.revenues=data.revs;

    })

    this._temporadaService.getBlackout().subscribe((data:any)=>{
      console.log(data)
      this.blackouts=data.blackouts;
    })

    this._hotelService.getHoteles().subscribe((data:any)=>{
      this.hoteles=data.hoteles;
      this.select_hoteles=this._utileriasService.covertToSelect(data.hoteles);
      this.select_hoteles.unshift({label:"Todos los hoteles",value: null})
    })
  }

  addTemporada(){
    let temporada = this.temporada_form.value;
    this.temporada_on=temporada;
    this.action_on='nuevo'
    this.type_on="NORMAL"
    this._temporadaService.addTemporada(temporada).subscribe((data:any)=>{
      console.log(data)
      this.success_modal.show();
      this.update()
      this.temporada_form.reset()

    }, err=>{
      if(err.status==500){
        this.fecha_error.show()
      }else if(err.status==0){
        this.connection_error.show()
      }

    })
  }

  addRevenue(){
    let revenue = this.temporada_form.value;
    this.temporada_on=revenue;
    this.action_on='nuevo'
    this.type_on="REV"
    this._temporadaService.addRevenue(revenue).subscribe((data:any)=>{
      console.log(data)
      this.success_modal.show();
      this.update()
      this.temporada_form.reset()

    }, err=>{
      if(err.status==500){
        this.fecha_error.show()
      }else if(err.status==0){
        this.connection_error.show()
      }

    })
  }

  addBlackout(){
    let blackout = this.temporada_form.value;
    this.temporada_on=blackout;
    this.action_on='nuevo'
    this.type_on="BOUT"
    this._temporadaService.addBlackout(blackout).subscribe((data:any)=>{
      console.log(data);
      this.success_modal.show();
      this.update()
      this.delete_modal.hide();
      this.temporada_form.reset()
    }, err=>{
      if(err.status==500){
        this.fecha_error.show()
      }else if(err.status==0){
        this.connection_error.show()
      }

    })
  }

  selectItemToEdit(item){
    this.on_edit=true;
    this.temporada_on=item;
    this.action_on='editar'
    this.id_on_edit=item._id;
    console.log(item);
    this.nombre.setValue(item.nombre);
    this.fechaInicio.setValue(this._utileriasService.getFormatDate(item.fechaInicio))
    this.fechaFinal.setValue(this._utileriasService.getFormatDate(item.fechaFinal))
    this.hotel.setValue(item.idhotel)

  }

  editTemporada(temporada:any){
    let edited_temporada={
      fechaInicio:this.fechaInicio.value,
      fechaFinal: this.fechaFinal.value,
      idhotel: this.hotel.value,
      nombre: this.nombre.value,
      _id:temporada._id
    }
    this.type_on=temporada.tipo;
    this._temporadaService.updateTemporada(edited_temporada).subscribe(data=>{
      console.log(data);
      this.update();
      this.success_modal.show();
      this.id_on_edit=null;
      this.on_edit=false;
    })
  }

  deleteTemporada(){
    this.delete_modal.hide()
    this._temporadaService.deleteTemporada(this.temporada_on._id).subscribe(data=>{
      console.log(data)
      this.update()
      this.modal_success_delete.show()
    })
  }

  confirmDelete(temporada){
    this.type_on=temporada.tipo;
    this.temporada_on=temporada;
    this.delete_modal.show()

  }

  initControls(){
    this.on_edit=false;
    this.id_on_edit=null;
    this.temporada_form.reset()
  }



}
