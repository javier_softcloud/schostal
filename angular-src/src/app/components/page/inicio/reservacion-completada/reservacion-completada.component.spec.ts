import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservacionCompletadaComponent } from './reservacion-completada.component';

describe('ReservacionCompletadaComponent', () => {
  let component: ReservacionCompletadaComponent;
  let fixture: ComponentFixture<ReservacionCompletadaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservacionCompletadaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservacionCompletadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
