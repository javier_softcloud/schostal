import { Component, OnInit } from '@angular/core';
import {ReservacionService} from "../../../../services/reservacion.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-reservacion-completada',
  templateUrl: './reservacion-completada.component.html',
  styleUrls: ['./reservacion-completada.component.css']
})
export class ReservacionCompletadaComponent implements OnInit {

  constructor( public _reservacionService:ReservacionService,
               public router:Router) {
    this._reservacionService.is_on_reservarion=true;
  }

  ngOnInit() {
  }

  gotoReservaciones(){
    this.router.navigate(['home','perfil','reservaciones'])
  }

  gotoInicio(){
    this.router.navigate(['home','inicio'])

  }
}
