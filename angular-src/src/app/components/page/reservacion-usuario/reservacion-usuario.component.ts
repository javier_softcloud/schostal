import {Component, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ReservacionService} from "../../../services/reservacion.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../../guards/auth.service";
import {UtileriaService} from "../../../services/utileria.service";
import {HotelService} from "../../../services/hotel.service";
import { IPayPalConfig, ICreateOrderRequest } from 'ngx-paypal';
import {IMyOptions} from "ng-uikit-pro-standard";
import {PageComponent} from "../page.component";


@Component({
  selector: 'app-reservacion-usuario',
  templateUrl: './reservacion-usuario.component.html',
  styleUrls: ['./reservacion-usuario.component.css']
})
export class ReservacionUsuarioComponent implements OnInit {

  @ViewChild('success') modal_success:any;
  @ViewChild('resumen_reservacion') resumen_reservacion:any;
  @ViewChild(PageComponent) pagina_component:PageComponent;
  @ViewChild('login_modal') login_modal:any;
  @ViewChild('newCCard') card_new_card:any;


  public payPalConfig?: IPayPalConfig;
  public number_people:any[] = [
    {value: "01", label: "1"},
    {value: "02", label: "2"},
    {value: "03", label: "3"},
    {value: "04", label: "4"},
    {value: "05", label: "5"},
    {value: "06", label: "6"}
  ];

  total_price:any;
  active_paypal:boolean=false;
  search_reservation:any={
    opciones:[{
      ops:[{
        total:null,
        cantidad:null,
        ocupacion:{camas:[]}
      }],
      ocup:{
        adultos:null,
        ninos:null
      }
    }],
    info:{
      name:null
    }
  };
  hotel_select=[];
  reserva_inicial:any;
  search_form:FormGroup;
  opciones:any=[];
  public date_options: IMyOptions;
  public num_hab:FormControl;
  public number_habs=[];
  public num_adultos:FormControl;
  public num_ninos:FormControl;
  public checkin:FormControl;
  public checkout:FormControl;
  public idhotel:FormControl;
  public number_habs_selected:FormControl;
  public credit_card_form:FormGroup;
  public hoteles=[];
  public habs_aux=[];
  public habitaciones_selected=[];
  public today:Date;
  public array_controls_habs:FormArray;
  public credit_cards:any = [];
  public valid_credit_card:boolean=true;
  public card_token:FormControl;
  public terminos:FormControl;
  public cvc: FormControl;

  constructor(public _reservationService:ReservacionService,
              public activatedRoute:ActivatedRoute,
              public _authService:AuthService,
              public _utileriasService:UtileriaService,
              public _hotelService:HotelService,
              public router:Router,
              private formBuilder:FormBuilder,
              public _reservacionService:ReservacionService) {
    this.today=new Date();
    this.terminos = new FormControl('',Validators.required)
    this.card_token = new FormControl('',Validators.required)
    this.number_habs_selected= new FormControl('');
    this.idhotel= new FormControl('');
    this.checkin= new FormControl('');
    this.checkout= new FormControl('');
    this.num_hab= new FormControl('');
    this.card_token = new FormControl('');
    this.cvc = new FormControl('',Validators.required)
    this.array_controls_habs= this.formBuilder.array([]);
    this.date_options={
      disableUntil:{year:this.today.getFullYear(),month:this.today.getMonth()+1,day:this.today.getDate()}
    };
    this.reserva_inicial= JSON.parse(localStorage.getItem('reservation_object'));
    this.idhotel.setValue(this.reserva_inicial.idhotel);
    this.checkin.setValue(this.reserva_inicial.checkin);
    this.checkout.setValue(this.reserva_inicial.checkout);
    this.number_habs_selected.setValue(this.reserva_inicial.habs.length)
    this.habs_aux =[];
    this.habs_aux.push({
        label: this.number_habs_selected.value + " habitaciones",
        value:1
      }
    )
    this.num_hab.setValue(1)


    console.log('Reservación Inicial', this.reserva_inicial);
    this._reservationService.is_on_reservarion=true;
    this.search_form = new FormGroup({
      idhotel: new FormControl('',Validators.required),
      adultos: new FormControl('',Validators.required),
      ninos:new FormControl('',Validators.required),
      checkin:new FormControl('',Validators.required),
      checkout:new FormControl('',Validators.required)
    })
    this.credit_card_form= new FormGroup({
      cvcNumber: new FormControl('',Validators.required),
      expDate: new FormControl('',Validators.required),
      ccNumber:new FormControl('',Validators.required),
      ccClientName:new FormControl('',Validators.required)
    })

    this._hotelService.getPublicHoteles().subscribe((data:any)=>{
      this.hoteles=data.hoteles
      this.hotel_select=this.convertToSelect(this.hoteles);
    })

  }

  ngOnInit() {
    this.updateReservationObject();
  }

  async updateReservationObject(){
      this._reservationService.searchReserva(this.reserva_inicial).subscribe((data)=>{
        this.search_reservation = data;
      });
  }

  getNoches(){
    if(this.search_reservation.opciones[0].ops.length==0)
      return 0
    else
      return this.search_reservation.opciones[0].ops[0].total/this.search_reservation.opciones[0].ops[0].precio
  }

  searchReserva(){
    this._reservacionService.activated_reservation={
      idhotel:this.idhotel.value,
      checkin:this.checkin.value,
      habs:this.parseIntArrayHabs(this.array_controls_habs.value),
      checkout:this.checkout.value
    }

    console.log('Reservation Object',this._reservacionService.activated_reservation);

    localStorage.setItem('reservation_object',this._reservacionService.activated_reservation);
    this._reservationService.searchReserva(this._reservacionService.activated_reservation).subscribe((reservation:any)=>{
      console.log(reservation)
      this.search_reservation=reservation;
    })
  }

  parseIntArrayHabs(array:any[]){
    console.log(array)
    for(let i=0; i<array.length; i++){
      array[i].ninos=parseInt(array[i].ninos)
      array[i].adultos=parseInt(array[i].adultos)
    }
    console.log(array)


    return array;
  }

  showNumberHabs(number_habs){
    console.log(number_habs)
    this.array_controls_habs=this.formBuilder.array([]);
    this.number_habs=[];
    for(let i=0; i < number_habs; i++ ){
      this.array_controls_habs.push(this.formBuilder.group({
        adultos: this.formBuilder.control(''),
        ninos: this.formBuilder.control('')
      }))
      this.number_habs.push(i)
    }
    console.log(this.array_controls_habs)
  }



  private initConfig(): void {

  }

  openHabsSelect(){

  }



  onClosePopHabs(){
    this.habs_aux=[];
    this.habs_aux.push({
        label: this.number_habs_selected.value + " habitaciones",
        value:1
      }
    )
    this.num_hab.setValue(1);
    console.log(this.habs_aux,this.num_hab)
  }

  buildReservationObject(){
      let idhotel=this.activatedRoute.snapshot.paramMap.get('idhotel');
      let checkin=this.activatedRoute.snapshot.paramMap.get('checkin');
      let checkout=this.activatedRoute.snapshot.paramMap.get('checkout');
      let adultos=parseInt(this.activatedRoute.snapshot.paramMap.get('num_adultos'));
      // let habs=this.activatedRoute.snapshot.paramMap.get('num_hab');
     let search_object={
       idhotel:idhotel,
       checkin:checkin,
       checkout:checkout,
       personas:adultos
     }
    return search_object;
  }

  ngOnDestroy(){
    this._reservationService.is_on_reservarion=false;
  }

  convertToSelect(array:any[]){

    let array_for_select:any[]=[];
    for(let item of array){
      array_for_select.push({
        value:item._id,
        label:item.nombre
      })
    }

    return array_for_select
  }

  getArrayOfHabsid(){
    let array_habs:any[]=[];
    for(let hab of this.habitaciones_selected)
      array_habs.push(hab.habitacion._id)

    return array_habs;
  }



  doReservation(){
   let reservation = JSON.parse(localStorage.getItem('reservation_object'));
   console.log(reservation+'')
    let ccToken:string = this.card_token.value;
    let idusuario=this._authService.getUserId();
    let nueva_reservacion={
      "idhotel":reservation.idhotel,
      "nombreHotel":this._utileriasService.getPropertyById('nombre',this.hoteles,reservation.idhotel),
      "habs":this.getArrayOfHabsid(),
      "ocupacionAdultos":this.getOcupacionAdultos(),
      "ocupacionNinos":this.getOcupacionNinos(),
      "total":this.getTotal(),
      "idusuario":idusuario,
      "checkin":reservation.checkin,
      "checkout":reservation.checkout,
      "creditCard":ccToken,
      "cvc": this.cvc.value,
      "guests":[]

    }

    console.log(nueva_reservacion)

    this._reservationService.doReserva(nueva_reservacion).subscribe(data=>{
      console.log("Se ha comunicado al servidor la reserva",data)
      this.router.navigate(['home','reservacion-completada'])
    })
  }

  getOcupacionAdultos(){
    let reservation:any = JSON.parse(localStorage.getItem('reservation_object'));
    console.log(reservation)
    let total:number=0;
    for(let hab of reservation.habs){
      total = hab.adultos + total;
    }

    return total;
  }

  getOcupacionNinos(){
    let total:number=0;
    let reservation:any = JSON.parse(localStorage.getItem('reservation_object'));
    for(let hab of reservation.habs){
      total = hab.ninos + total;
    }

    return total;
  }

  pushHanitacionToReserva(numero_opcion:number,habitacion:any){
    if(this.ifOptionIsSelected(numero_opcion))
      this.habitaciones_selected.push({
        opcion:numero_opcion,
        habitacion:habitacion
      })
    else
      for(let i=0; i<this.habitaciones_selected.length; i++){
        if(this.habitaciones_selected[i].opcion==numero_opcion)
          this.habitaciones_selected[i].habitacion=habitacion;
      }

    console.log(this.habitaciones_selected)
  }

  popHabitacionToReserva(habitacion:any){
    for(let i=0; i<this.habitaciones_selected.length;i++){
      if(this.habitaciones_selected[i].habitacion._id==habitacion._id)
        this.habitaciones_selected.splice(i,1);
    }

    console.log(this.habitaciones_selected)

  }

  IfHabIsSelected(habitacion:any, numero_opcion:number){
    for(let hab of this.habitaciones_selected){
      if(hab._id==habitacion._id && numero_opcion!=hab.opcion)
        return true;
      return false;
    }
  }

  ifHabitacionIsInArray(habitacion_id:string,numero_opcion:number){
    for(let habitacion of this.habitaciones_selected)
      if(habitacion.habitacion._id==habitacion_id && numero_opcion==habitacion.opcion)
        return true;
    return false;
  }

  ifOptionIsSelected(numero_opcion:number){
    for(let habitacion of this.habitaciones_selected)
      if(habitacion.opcion==numero_opcion)
        return false

    return true
  }

  changeSelectBox($event,numero_opcion:number,habitacion:any){
    if($event.checked)
      this.pushHanitacionToReserva(numero_opcion, habitacion);
    else
      this.popHabitacionToReserva(habitacion);
  }

  getTotal(){
    let total=0;
    for(let opcion of this.habitaciones_selected)
      total = opcion.habitacion.total + total;
    this.total_price=total;

    return total;
  }

  resumenReserva(){
    this.resumen_reservacion.show();
  }

  onOpenReservationConfirm(){
    if(this._authService.isAuthenticatedUser())
      this._reservacionService.getCCards().subscribe((card_list:any)=>{
        this.credit_cards = card_list;
        console.log("Credits Card",this.credit_cards)
      })
  }

  updateCreditCards(){
    this._reservacionService.getCCards().subscribe((cards)=>{
      this.credit_cards = cards;
    })
  }

  addCard(){
    console.log(this.credit_card_form.value)
    let credit_card = this.credit_card_form.value;
    this._reservacionService.addCreditCard(credit_card).subscribe((card)=>{
      this.card_new_card.toggle();
      this.credit_card_form.reset();
      this.valid_credit_card = true;
      this.updateCreditCards();
      console.log(card)
    },(err)=>{
      this.valid_credit_card = false;
    })
  }

  getNumHabitaciones(){
    return this.habitaciones_selected.length;
  }

  validConfirmReservation(){
    return this.terminos.valid && this.card_token.valid
  }
}
