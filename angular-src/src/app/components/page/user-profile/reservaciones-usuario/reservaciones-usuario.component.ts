import { Component, OnInit } from '@angular/core';
import {ReservacionService} from "../../../../services/reservacion.service";
import {HotelesComponent} from "../../../admin/dashboard-admin/hoteles/hoteles.component";
import {HotelService} from "../../../../services/hotel.service";
import {UtileriaService} from "../../../../services/utileria.service";

@Component({
  selector: 'app-reservaciones-usuario',
  templateUrl: './reservaciones-usuario.component.html',
  styleUrls: ['./reservaciones-usuario.component.css']
})
export class ReservacionesUsuarioComponent implements OnInit {

  public reservaciones:any=[];
  public hoteles:any=[];
  constructor(public _reservacionService:ReservacionService,
              public _hotelService:HotelService,
              public _utileriasService:UtileriaService) {
    this._reservacionService.getReservacionUsuario().subscribe((data:any)=>{
      this.reservaciones=data.reservaciones;
      console.log('Reservaciones',this.reservaciones);
    })
    this.init()
  }

  ngOnInit() {
  }

  init(){
    this._hotelService.getPublicHoteles().subscribe((hoteles:any)=>{
      this.hoteles= hoteles.hoteles;
      console.log('Hoteles',this.hoteles);

    })

  }

  getHotelName(idhotel:string){
    return this._utileriasService.getPropertyById('nombre',this.hoteles,idhotel)
  }

}
