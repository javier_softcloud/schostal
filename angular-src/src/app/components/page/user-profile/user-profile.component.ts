import {Component, OnInit, ViewChild} from '@angular/core';
import {ClienteService} from '../../../services/cliente.service';
import {environment} from '../../../../environments/environment';
import {AuthService} from '../../../guards/auth.service';
import {FormControl} from '@angular/forms';
import {UploadDownloadFilesService} from '../../../services/upload-download-files.service';
import {ReservacionService} from '../../../services/reservacion.service';
import { FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  usuario: any = {
    nombre: null,
    apPaterno: null,
    apMaterno: null
  };
  temp_image: any;
  servidor: string;
  @ViewChild('imagenUploadModal') imagen_upload_modal: any;
  image_file: FormControl;
  selected_file: any;
  uploader: FileUploader = new FileUploader({ url: `${environment.api_host}/upload/usuario/5c623cb32133086d05e71f03`,
                                            itemAlias: 'img', authToken: localStorage.getItem('token_user')});

  constructor(private _userService: ClienteService,
              private _uploadDonwloadFiles: UploadDownloadFilesService,
              private _reservationService: ReservacionService) {
    this._reservationService.is_on_reservarion = true;
    this.image_file = new FormControl('');
    this.servidor = environment.api_host;
    this._userService.getSingleUser().subscribe((data: any) => {
      this.usuario = data.usuario;
      console.log(localStorage.getItem('token_user'));
    });
  }

  ngOnInit() {
    this.uploader.authToken = localStorage.getItem('token_user');
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('ImageUpload:uploaded:', item, status, response);
      alert('Se subió');
    };
  }

  onSelectFile($event) {
    console.log($event);
  }

  openModalImageUpload() {
    this.imagen_upload_modal.show();
  }

}
