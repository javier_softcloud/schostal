import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ClienteService} from "../../../../services/cliente.service";
import {AuthService} from "../../../../guards/auth.service";
import {ModalDirective, ModalOptions} from "angular-bootstrap-md";


@Component({
  selector: 'app-editar-perfil',
  templateUrl: './editar-perfil.component.html',
  styleUrls: ['./editar-perfil.component.css']
})
export class EditarPerfilComponent implements OnInit {

  @ViewChild('success') succes:any;
  form_data_user:FormGroup;
  usuario:any;

  constructor( public _userService:ClienteService,
               public _authService:AuthService) {
    this.usuario=this._userService.getSingleUser().subscribe((data:any)=>{
      this.usuario=data.usuario;
      console.log(data)

      this.initUserForm()
      }
    )
    this.form_data_user=new FormGroup({
      _id:new FormControl('',Validators.required),
      nombre: new FormControl('',Validators.required),
      apPaterno:new FormControl('',Validators.required),
      apMaterno:new FormControl('',Validators.required),
      telefono:new FormControl(''),
      fechaNacimiento: new FormControl(''),
      email: new FormControl('')
    })


  }

  onChangeForm() {
    if (this.form_data_user.get('_id').value == this.usuario._id &&
      this.form_data_user.get('nombre').value == this.usuario.nombre &&
      this.form_data_user.get('apPaterno').value == this.usuario.apPaterno &&
      this.form_data_user.get('apMaterno').value == this.usuario.apMaterno &&
      this.form_data_user.get('email').value == this.usuario.email &&
      this.form_data_user.get('telefono').value == this.usuario[0].telefono &&
      this.form_data_user.get('fechaNacimiento').value == this.usuario.fechaNacimiento)
      return true;
    return false;
  }

  ngOnInit() {
  }

  initUserForm(){
    console.log(this.usuario.apMaterno);
    this.form_data_user.get('_id').setValue(this.usuario._id);
    this.form_data_user.get('nombre').setValue(this.usuario.nombre);
    this.form_data_user.get('apPaterno').setValue(this.usuario.apPaterno);
    this.form_data_user.get('apMaterno').setValue(this.usuario.apMaterno);
    this.form_data_user.get('email').setValue(this.usuario.email);
    this.form_data_user.get('telefono').setValue(this.usuario.telefono);
    this.form_data_user.get('fechaNacimiento').setValue(this.usuario.fechaNacimiento);
  }

  editUser(){
    this._userService.editSingleUser(this.form_data_user.value).subscribe(data=>{
      console.log(data)
      this.succes.show();
    })
  }

}
