import {Component, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../guards/auth.service";
import {ClienteService} from "../../services/cliente.service";
import {IMyOptions} from "ng-uikit-pro-standard";
import {Router} from "@angular/router";
import {ReservacionService} from "../../services/reservacion.service";
import {HotelService} from "../../services/hotel.service";

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})



export class PageComponent implements OnInit {

  login_form:FormGroup;
  signup_form:FormGroup;
  array_habs:FormArray;
  public today:Date;
  public user_invalid:boolean=false;
  public date_options: IMyOptions;
  public num_hab:FormControl;
  public number_habs=[];
  public num_adultos:FormControl;
  public num_ninos:FormControl;
  public checkin:FormControl;
  public checkout:FormControl;
  public idhotel:FormControl;
  public number_habs_selected:FormControl;
  public hoteles;
  public habs_aux=[];
  public hotel_select:any[]=[
    {value: "01", label: "Canún"}
  ];
  public form_habitaciones:FormGroup;
  public number_people:any[] = [
    {value: "0", label: "0"},
    {value: "1", label: "1"},
    {value: "2", label: "2"},
    {value: "3", label: "3"},
    {value: "4", label: "4"},
    {value: "5", label: "5"},
    {value: "6", label: "6"}
  ];
  ninos:any[]=[];
  array_controls_habs:FormArray;
  habs:FormArray;

  @ViewChild('login_modal') login_modal:any;


  constructor( public _authService:AuthService,
               private _userService:ClienteService,
               private router:Router,
               public _reservacionService:ReservacionService,
               private formBuilder: FormBuilder,
               public _hotelService:HotelService) {
    this.number_habs_selected= new FormControl('');
    this.array_controls_habs= this.formBuilder.array([]);
    this._hotelService.getPublicHoteles().subscribe(hoteles=>{
      this.hoteles=hoteles;
      this.hoteles=this.hoteles.hoteles;
      console.log(hoteles)
      this.hotel_select=this.convertToSelect(this.hoteles);
      console.log(this.hotel_select)
    })

    this.today=new Date();
    this.date_options={
      disableUntil:{year:this.today.getFullYear(),month:this.today.getMonth()+1,day:this.today.getDate()}
    };

    this.login_form= new FormGroup({
      email: new FormControl('',Validators.required),
      password: new FormControl('',Validators.required)
    })
    this.signup_form=new FormGroup({
      nombre: new FormControl('',Validators.required),
      apPaterno: new FormControl('',Validators.required),
      apMaterno: new FormControl('',Validators.required),
      email:new FormControl('',[Validators.required,Validators.pattern(/^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/)]),
      password: new FormControl('',[Validators.required,Validators.pattern(/(?=\w*\d)(?=\w*[a-z])\S{6,16}$/)]),
      confirm_password: new FormControl('',[Validators.required])
    })
    this.signup_form.get('confirm_password').setValidators([Validators.required, this.passwordValidate.bind(this.signup_form.controls['password'])]);


      this.num_hab= new FormControl('',Validators.required),
      this.num_adultos= new FormControl('',Validators.required),
      this.num_ninos= new FormControl('',Validators.required),
      this.checkin= new FormControl('',Validators.required),
      this.checkout= new FormControl('',Validators.required),
      this.idhotel= new FormControl('',Validators.required)

  }

  ngOnInit() {

  }


  passwordValidate(control: FormControl): { [s: string]: boolean } {
    let outControl: any = this;
    if (control.value != outControl.value) {
      return {password_validate: true};
    }

    return null;
  }


  getUserName(){
    return localStorage.getItem('user_name');
  }


  getEmailUser(){
    return localStorage.getItem('user_email');
  }

  isValidToken(){
    return this._authService.isAuthenticatedUser();
  }


  logout(){
    localStorage.clear();
  }

  gotoPerfil(){
    this.router.navigate(['home/perfil']);
  }

  resetForms(){
    this.signup_form.reset();
    this.login_form.reset();
    this.user_invalid=false;
  }

  changeChildren($event){
    this.ninos=Array(parseInt($event.value)).fill(1).map((x,i)=>i)
  }

  gotoReservar(){
    this.router.navigate(['/home','reservar'])
    this._reservacionService.activated_reservation = {
        idhotel:this.idhotel.value,
        checkin:this.checkin.value,
        habs:this.parseIntArrayHabs(this.array_controls_habs.value),
        checkout:this.checkout.value
    }

    localStorage.setItem('reservation_object',JSON.stringify(this._reservacionService.activated_reservation));

    console.log(this._reservacionService.activated_reservation);
  }

  validaCampos(){
    return     this.num_hab.valid
            && this.num_ninos.valid
            && this.num_adultos.valid
            && this.checkin.valid
            && this.checkout.valid
            && this.idhotel.valid
  }

  parseIntArrayHabs(array:any[]){
    console.log(array)
    for(let i=0; i<array.length; i++){
      array[i].ninos=parseInt(array[i].ninos)
      array[i].adultos=parseInt(array[i].adultos)
    }

    return array;
  }

  updateHabsReserva($event){
    this.form_habitaciones.controls['habs']=this.formBuilder.array([]);
    this.array_habs=this.form_habitaciones.get('habs') as FormArray;
    for(let i=0;i<$event;i++)
    this.array_habs.push(this.formBuilder.group({
      ninos:'',
      adultos: ''
    }))
    console.log(this.array_habs)
  }

  convertToSelect(array:any[]){
    let array_for_select:any[]=[];
    for(let item of array){
      array_for_select.push({
        value:item._id,
        label:item.nombre
      })
    }

    return array_for_select
  }

  openHabsSelect(){

  }

  showNumberHabs(number_habs){
    console.log(number_habs)
    this.array_controls_habs=this.formBuilder.array([]);
    this.number_habs=[];
    for(let i=0; i < number_habs; i++ ){
      this.array_controls_habs.push(this.formBuilder.group({
        adultos: this.formBuilder.control(''),
        ninos: this.formBuilder.control('')
      }))
      this.number_habs.push(i)
    }
    console.log(this.array_controls_habs)
  }

  onClosePopHabs(){
    this.habs_aux=[];
    this.habs_aux.push({
        label: this.number_habs_selected.value + " habitaciones",
        value:1
      }
    )
    this.num_hab.setValue(1);
    console.log(this.habs_aux,this.num_hab)
  }

  iniciaSesion(){
    this.login_modal.show();
  }

}
