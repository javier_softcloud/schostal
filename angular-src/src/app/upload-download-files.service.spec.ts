import { TestBed } from '@angular/core/testing';

import { UploadDownloadFilesService } from './services/upload-download-files.service';

describe('UploadDownloadFilesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UploadDownloadFilesService = TestBed.get(UploadDownloadFilesService);
    expect(service).toBeTruthy();
  });
});
