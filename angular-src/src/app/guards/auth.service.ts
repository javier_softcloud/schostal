import { Injectable } from '@angular/core';
import { JwtHelperService} from '@auth0/angular-jwt';
import {environment} from "../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  servidor:string=environment.api_host;
  jwtHelper=new JwtHelperService();


  constructor(private http:HttpClient) { }


  public isAuthenticatedUser(): boolean {
    const token = localStorage.getItem('token_user');
    return !this.jwtHelper.isTokenExpired(token);
  }


  public isAuthenticatedAdmin(): boolean {
    const token = localStorage.getItem('token_admin');
    return !this.jwtHelper.isTokenExpired(token);
  }




  public loginUser(user:Object){
      let url=this.servidor+"/cliente/login";
      let body=user;
      let headers =new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept':'application/json'
      });
      return this.http.post(url,body,{headers})
  }

  public loginRoot(token:number){
    let url=this.servidor+"/admin/root";
    let body={token:token};
    console.log(body);
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json'
    });
    return this.http.post(url,body,{headers})
  }

  loginAdmin(admin:any){
    let url=this.servidor+"/admin/login";
    let body=admin;
    let headers =new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':'application/json'
    });
    return this.http.post(url,body,{headers})
  }

  getFullUserName(){
    let token_decoded=this.jwtHelper.decodeToken(localStorage.getItem('token_user')).usuario;
    return token_decoded.name +" "+ token_decoded.apPaterno +" "+ token_decoded.apMaterno
  }

  getUserName(){
    let token_decoded=this.jwtHelper.decodeToken(localStorage.getItem('token_user')).usuario;
    return token_decoded.nombre
  }

  getUserEmail(){
    let token_decoded=this.jwtHelper.decodeToken(localStorage.getItem('token_user')).usuario;
    return token_decoded.email
  }

  getAdminName(){
    let token_decoded=this.jwtHelper.decodeToken(localStorage.getItem('token_admin')).admin;
    return token_decoded.name
  }

  getAdminFullName(){
    let token_decoded=this.jwtHelper.decodeToken(localStorage.getItem('token_admin')).admin;
    return token_decoded.name+" "+ token_decoded.apPaterno +" "+ token_decoded.apMaterno
  }

  getAdminRol(){
    let token_decoded=this.jwtHelper.decodeToken(localStorage.getItem('token_admin')).admin;
    console.log(token_decoded)
    return token_decoded.role;
  }

  getAdminId(){
    let token_decoded=this.jwtHelper.decodeToken(localStorage.getItem('token_admin')).admin;
    return token_decoded._id;
  }

  getUserId(){
    let token_decoded=this.jwtHelper.decodeToken(localStorage.getItem('token_user')).usuario;
    console.log(token_decoded)
    return token_decoded._id;
  }



}
