// Angular components
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FileUploadModule } from 'ng2-file-upload';
import { CreditCardDirectivesModule } from 'angular-cc-library';


// Rutas
import {APP_ROUTING} from './app.routes';


// Components
import { PageComponent } from './components/page/page.component';
import { AdminComponent } from './components/admin/admin.component';
import { InicioComponent } from './components/page/inicio/inicio.component';
import { HomeComponent } from './components/home/home.component';
import { NosotrosComponent } from './components/page/nosotros/nosotros.component';
import { ServiciosComponent } from './components/page/servicios/servicios.component';
import { ContactoComponent } from './components/page/contacto/contacto.component';
import { ReservacionComponent } from './components/page/reservacion/reservacion.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { LoginAdminComponent } from './components/admin/login-admin/login-admin.component';
import { DashboardAdminComponent } from './components/admin/dashboard-admin/dashboard-admin.component';




// Servicios
import {ContactoService} from './services/contacto.service';


// Plugins
import { AgmCoreModule } from '@agm/core';
import {JwtHelperService, JwtModule} from '@auth0/angular-jwt';



// Bootstrap theme
import { MDBBootstrapModulesPro, SidenavModule, AccordionModule,
  TabsModule, InputsModule, WavesModule,
  ModalModule, ButtonsModule,
  NavbarModule } from 'ng-uikit-pro-standard';
import { MDBSpinningPreloader } from 'ng-uikit-pro-standard';
import {AuthService} from './guards/auth.service';
import {AuthGuard} from './guards/auth.guard';
import {AuthAdminGuard} from './guards/auth-admin.guard';
import { AdministradoresComponent } from './components/admin/dashboard-admin/administradores/administradores.component';
import { ClientesComponent } from './components/admin/dashboard-admin/clientes/clientes.component';
import { MensajesContactoComponent } from './components/admin/dashboard-admin/mensajes-contacto/mensajes-contacto.component';
import { ReservacionesComponent } from './components/admin/dashboard-admin/reservaciones/reservaciones.component';
import {ClienteService} from './services/cliente.service';
import { ModalDeleteItemComponent } from './components/modal-delete-item/modal-delete-item.component';
import { ModalDeleteArrayComponent } from './components/modal-delete-array/modal-delete-array.component';
import { SureDeleteComponent } from './components/sure-delete/sure-delete.component';
import {AdminService} from './services/admin.service';
import { HotelesComponent } from './components/admin/dashboard-admin/hoteles/hoteles.component';
import { HabitacionesComponent } from './components/admin/dashboard-admin/habitaciones/habitaciones.component';
import { ActivosCamasComponent } from './components/admin/dashboard-admin/activos-camas/activos-camas.component';
import {HabitacionService} from './services/habitacion.service';
import {HotelService} from './services/hotel.service';
import {ActivoCamaService} from './services/activo-cama.service';
import { UserProfileComponent } from './components/page/user-profile/user-profile.component';
import { EditarPerfilComponent } from './components/page/user-profile/editar-perfil/editar-perfil.component';
import { ReservacionUsuarioComponent } from './components/page/reservacion-usuario/reservacion-usuario.component';
import {ReservacionService} from './services/reservacion.service';
import { ConsumiblesComponent } from './components/admin/dashboard-admin/consumibles/consumibles.component';
import {AngularFileUploaderModule} from 'angular-file-uploader';
import {DndModule} from 'ngx-drag-drop';
import {CalendarModule, DateAdapter} from 'angular-calendar';
import {adapterFactory} from 'angular-calendar/date-adapters/date-fns';
import { TemporadasComponent } from './components/admin/dashboard-admin/temporadas/temporadas.component';
import {NgxPayPalModule} from 'ngx-paypal';
import { ReservacionCompletadaComponent } from './components/page/inicio/reservacion-completada/reservacion-completada.component';
import { LoginComponent } from './login/login.component';
import { ReservacionesUsuarioComponent } from './components/page/user-profile/reservaciones-usuario/reservaciones-usuario.component';
import { ClienteComponent } from './components/admin/dashboard-admin/clientes/cliente/cliente.component';

@NgModule({
  declarations: [
    AppComponent,
    PageComponent,
    AdminComponent,
    InicioComponent,
    HomeComponent,
    NosotrosComponent,
    ServiciosComponent,
    ContactoComponent,
    ReservacionComponent,
    LoginAdminComponent,
    DashboardAdminComponent,
    AdministradoresComponent,
    ClientesComponent,
    MensajesContactoComponent,
    ReservacionesComponent,
    ModalDeleteItemComponent,
    ModalDeleteArrayComponent,
    SureDeleteComponent,
    HotelesComponent,
    HabitacionesComponent,
    ActivosCamasComponent,
    ReservacionUsuarioComponent,
    ActivosCamasComponent,
    ConsumiblesComponent,
    TemporadasComponent,
    UserProfileComponent,
    EditarPerfilComponent,
    ActivosCamasComponent,
    ConsumiblesComponent,
    ConsumiblesComponent,
    ReservacionCompletadaComponent,
    LoginComponent,
    ReservacionesUsuarioComponent,
    ClienteComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    TabsModule,
    CreditCardDirectivesModule,
    FileUploadModule,
    InputsModule,
    NgxPayPalModule,
    NavbarModule,
    ModalModule,
    ButtonsModule,
    JwtModule,
    MDBBootstrapModulesPro.forRoot(),
    WavesModule,
    HttpClientModule,
    SidenavModule,
    AgmCoreModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    DndModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBp9oQFtlc8ldsDWQcCqQ7yPCinm0LRoFw'
    }),
    AngularFileUploaderModule
  ],
  providers: [
    ContactoService,
    MDBSpinningPreloader,
    AuthService,
    ClienteService,
    AuthGuard,
    AuthAdminGuard,
    HttpClient,
    JwtHelperService,
    AdminService,
    HabitacionService,
    HotelService,
    ActivoCamaService,
    ReservacionService
  ],
  schemas: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
