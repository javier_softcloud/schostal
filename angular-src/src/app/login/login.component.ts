import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ClienteService} from "../services/cliente.service";
import {AuthService} from "../guards/auth.service";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChild('error_modal') error_modal:any;
  @ViewChild('success_login') success_login:any;
  @ViewChild('success_signup') success_signup:any;
  @ViewChild('login') login_modal:any;


  public login_form:FormGroup;
  public signup_form:FormGroup;
  public user_invalid:boolean=false;


  constructor(private _userService:ClienteService,
              private _authService:AuthService) {
    this.login_form= new FormGroup({
      email: new FormControl('',Validators.required),
      password: new FormControl('',Validators.required)
    })
    this.signup_form=new FormGroup({
      nombre: new FormControl('',Validators.required),
      apPaterno: new FormControl('',Validators.required),
      apMaterno: new FormControl('',Validators.required),
      email:new FormControl('',[Validators.required,Validators.pattern(/^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/)]),
      password: new FormControl('',[Validators.required,Validators.pattern(/(?=\w*\d)(?=\w*[a-z])\S{6,16}$/)]),
      confirm_password: new FormControl('',[Validators.required])
    })
    this.signup_form.get('confirm_password').setValidators([Validators.required, this.passwordValidate.bind(this.signup_form.controls['password'])]);

  }

  ngOnInit() {
  }


  resetForms(){
    this.signup_form.reset();
    this.login_form.reset();
    this.user_invalid=false;
  }

  setLogin(){
    this._authService.loginUser(this.login_form.value).subscribe((data:any)=>{
      localStorage.setItem("token_user",data.token)
      localStorage.setItem("user_name",data.usuario.nombre);
      localStorage.setItem("user_email",data.usuario.email);
      this.login_modal.hide();
      this.success_login.show();
      this.signup_form.reset();
    },err=>{
      if(err.status==401){
        this.user_invalid=true;
      }else{
        this.error_modal.show();
      }
      console.log(err)
    })
  }

  passwordValidate(control: FormControl): { [s: string]: boolean } {
    let outControl: any = this;
    if (control.value != outControl.value) {
      return {password_validate: true};
    }

    return null;
  }

  setSignup(){
    this._userService.signupUser(this.signup_form.value).subscribe((data:any)=>{
      this.success_signup.show();
      this.login_modal.hide();
      this.signup_form.reset();
    },err=>{
      console.log(err)
      this.error_modal.show();
    })
  }

}



